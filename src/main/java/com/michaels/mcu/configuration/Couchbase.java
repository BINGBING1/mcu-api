package com.michaels.mcu.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.convert.CustomConversions;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.config.BeanNames;
import org.springframework.data.couchbase.core.mapping.CouchbaseMappingContext;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

@Profile("!disableVault")
@EnableCouchbaseRepositories
@Configuration
class Couchbase extends AbstractCouchbaseConfiguration {

    @Value("${spring.couchbase.bucket}")
    private String bucketName;

    @Value("${spring.couchbase.username}")
    private String username;

    @Value("${spring.couchbase.password}")
    private String password;

    @Value("${spring.couchbase.connection-string}")
    private String ip;

    @Value("${spring.couchbase.autoIndex}")
    private boolean autoIndexCreation;

    @Override
    public String getConnectionString() {
        return this.ip;
    }

    @Override
    public String getUserName() {
        return this.username;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getBucketName() {
        return this.bucketName;
    }

    @Override
    public boolean autoIndexCreation() {
        return this.autoIndexCreation;
    }

    @Override
    @Bean(name = BeanNames.COUCHBASE_MAPPING_CONTEXT)
    public CouchbaseMappingContext couchbaseMappingContext(
            @Qualifier("couchbaseCustomConversions") CustomConversions customConversions)
            throws Exception {
        CouchbaseMappingContext mappingContext = new CouchbaseMappingContext();
        mappingContext.setInitialEntitySet(this.getInitialEntitySet());
        mappingContext.setSimpleTypeHolder(customConversions.getSimpleTypeHolder());
        mappingContext.setFieldNamingStrategy(this.fieldNamingStrategy());
        mappingContext.setAutoIndexCreation(this.autoIndexCreation());
        return mappingContext;
    }
}
