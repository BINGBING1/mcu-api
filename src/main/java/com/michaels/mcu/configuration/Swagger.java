package com.michaels.mcu.configuration;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;

@Configuration
@OpenAPIDefinition(
    info =
        @Info(
            title = "Michaels User module",
            version = "v1.0.0",
            description = "User Api Documentation",
            contact =
                @Contact(
                    name = "Michaels Tech Team",
                        email = "shenzhen_china@michaelsstores.onmicrosoft.com",
                        url = "https://michaels.atlassian.net/wiki/spaces/DSG/overview?homepageId=495550676")),
    servers = {
            @Server(url = "/api/"),
            @Server(url = "/")
    }
)
@SecurityScheme(
    name = "bearerAuth",
    type = SecuritySchemeType.HTTP,
    in = SecuritySchemeIn.HEADER,
    scheme = "bearer"
)
public class Swagger {
  
  @Bean
  public GroupedOpenApi userApi() {
    return GroupedOpenApi.builder()
        .group("UserApi")
        .packagesToScan("com.michaels.mcu.user")
        .build();
  }

  @Bean
  public GroupedOpenApi sellerStoreApi() {
    return GroupedOpenApi.builder()
        .group("SellerStoreApi")
        .packagesToScan("com.michaels.mcu.sellerstore")
        .build();
  }

}
