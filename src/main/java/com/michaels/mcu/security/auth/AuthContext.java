package com.michaels.mcu.security.auth;

public class AuthContext {
  
  public static final ThreadLocal<AuthContext> threadLocalInstanceHolder=new ThreadLocal<AuthContext>();
  
  public static void setAuthContext(AuthContext authContext) {
    threadLocalInstanceHolder.set(authContext);
  }
  
  public static AuthContext get() {
    return threadLocalInstanceHolder.get();
  }
  
  private Long userId;
  private String token;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
  
  
}
