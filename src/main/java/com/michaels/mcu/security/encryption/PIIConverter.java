package com.michaels.mcu.security.encryption;

import org.springframework.beans.factory.annotation.Value;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.AttributeConverter;
import javax.persistence.Convert;
import java.nio.ByteBuffer;
import java.security.Key;
import java.security.SecureRandom;
import java.util.Base64;

@Convert
public class PIIConverter implements AttributeConverter<String, String> {
    private final Key KEY;
    private final String TRANSFORMATION = "AES/GCM/NoPadding";
    private static final Integer GCM_TAG_LENGTH = 128;
    private static final Integer GCM_IV_LENGTH = 12;
    private static final String CHAR_ENC = "UTF-8";
    public PIIConverter(@Value("${PII.aes.secret}") final String key) {
        this.KEY = new SecretKeySpec(key.getBytes(), "AES");
    }
    @Override
    public String convertToDatabaseColumn(final String plaintText) {
        if (plaintText == null) {
            return null;
        } else {
            try {
                byte[] IV = new byte[GCM_IV_LENGTH];
                SecureRandom random = new SecureRandom();
                random.nextBytes(IV);

                final Cipher cipher = Cipher.getInstance(this.TRANSFORMATION);
                GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, IV);
                cipher.init(Cipher.ENCRYPT_MODE, KEY, gcmParameterSpec);
                byte[] plainTextBytes = plaintText.getBytes(CHAR_ENC);
                byte[] cipherText = cipher.doFinal(plainTextBytes);

                ByteBuffer byteBuffer = ByteBuffer.allocate(4 + IV.length + cipherText.length);
                byteBuffer.putInt(IV.length);
                byteBuffer.put(IV);
                byteBuffer.put(cipherText);
                return Base64.getEncoder().encodeToString(byteBuffer.array());
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }
    @Override
    public String convertToEntityAttribute(final String cipherMessage) {
        if (cipherMessage == null) {
            return null;
        } else {
            try {
                byte[] decoded = Base64.getDecoder().decode(cipherMessage);
                ByteBuffer byteBuffer = ByteBuffer.wrap(decoded);
                int lengthOfIV = byteBuffer.getInt();
                byte[] IV = new byte[lengthOfIV];
                byteBuffer.get(IV);
                byte[] cipherText = new byte[byteBuffer.remaining()];
                byteBuffer.get(cipherText);

                final Cipher cipher = Cipher.getInstance(this.TRANSFORMATION);
                GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, IV);
                cipher.init(Cipher.DECRYPT_MODE, KEY, gcmParameterSpec);
                byte[] plainTextBytes = cipher.doFinal(cipherText);
                return new String(plainTextBytes, CHAR_ENC);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
