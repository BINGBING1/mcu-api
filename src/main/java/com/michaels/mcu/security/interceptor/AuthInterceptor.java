package com.michaels.mcu.security.interceptor;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.michaels.mcu.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.michaels.mcu.security.auth.AuthContext;
import com.michaels.mcu.shared.util.StringUtils;
import com.michaels.mcu.user.entity.UserDeviceSession;

@WebFilter("/*")
@Component
public class AuthInterceptor extends HttpFilter {
  
  private static final String BEARER_PREFIX="Bearer ";

  @Autowired
  private UserService userService;
  
  @Override
  protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    
    // attach the AuthContext to current thread, if any
    try {
      String token=request.getHeader("Authorization");
      if(StringUtils.startsWithIgnoreCase(token, BEARER_PREFIX)) {
        token=token.substring(BEARER_PREFIX.length());
      }

      UserDeviceSession userDeviceSession= userService.findUserDeviceSessionByTokenOptional(token).orElse(null);
      if(userDeviceSession!=null && !userDeviceSession.hasExpired()) {
        AuthContext authContext = new AuthContext();
        authContext.setUserId(userDeviceSession.getUserId());
        authContext.setToken(token);

        // refresh the last operate time
        LocalDateTime now=LocalDateTime.now();
        if(userDeviceSession.getLastOperateTime()==null
                || now.plusMinutes(-10).isAfter(userDeviceSession.getLastOperateTime())) {
          userService.refreshUserDeviceSession(userDeviceSession);
        }
        AuthContext.setAuthContext(authContext);
      } else {
        AuthContext.setAuthContext(null);
      }
    } catch (Exception e) {
      // ignore
      AuthContext.setAuthContext(null);
    }
    
    try {
      // process
      super.doFilter(request, response, chain);
      
    } finally {
      // clean
      AuthContext.setAuthContext(null);
    }
  }
  
}
