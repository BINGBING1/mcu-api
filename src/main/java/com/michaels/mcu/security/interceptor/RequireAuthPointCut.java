package com.michaels.mcu.security.interceptor;

import com.michaels.mcu.shared.common.BusinessCode;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.michaels.mcu.security.auth.AuthContext;
import com.michaels.mcu.shared.exception.InvalidCredentialsException;

@Aspect
@Component
public class RequireAuthPointCut {
  
  @Pointcut("@annotation(com.michaels.mcu.security.annotation.RequireAuth)")
  public void requireAuthMethods() {
    
  }
  
  @Before("requireAuthMethods()")
  public void requireAuthMethodsBefore(JoinPoint joinPoint) {
    checkRequireAuth(joinPoint);
  }
  

  @Pointcut("@within(com.michaels.mcu.security.annotation.RequireAuth)")
  public void requireAuthClasses() {
    
  }
  
  @Before("requireAuthClasses()")
  public void requireAuthClassesBefore(JoinPoint joinPoint) {
    checkRequireAuth(joinPoint);
  }
  

  private void checkRequireAuth(JoinPoint joinPoint) {
    AuthContext authContext=AuthContext.get();
    if(authContext==null) {
      throw new InvalidCredentialsException(BusinessCode.MCU_UNAUTHORIZED, "Unauthorized access");
    }
  }
}
