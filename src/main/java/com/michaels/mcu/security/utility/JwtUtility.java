package com.michaels.mcu.security.utility;

import java.util.Date;
import java.util.HashMap;
import java.util.function.Function;

import com.michaels.mcu.shared.common.BusinessCode;
import com.michaels.mcu.shared.exception.InvalidCredentialsException;
import com.michaels.mcu.shared.util.StringUtils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

// @TODO this is used for demo purpose, removed this class once security team's API is ready
public final class JwtUtility {
  private static final String SECRET = "13428049692442670";
  private static final Long ACCESS_VALIDITY = 5 * 365 * 24 * 60 * 60 * 1000L; // 5 years

  private JwtUtility() {
  }

  public static String generateJwtToken(Long userId) {
    return Jwts.builder()
        .setClaims(new HashMap<>())
        .setSubject(Long.toString(userId))
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + ACCESS_VALIDITY))
        .signWith(SignatureAlgorithm.HS256, SECRET)
        .compact();
  }

  public static String getTokenFromHeader(String authorizationHeader) {
    String jwt = null;
    if (authorizationHeader != null && StringUtils.startsWithIgnoreCase(authorizationHeader, "Bearer ")) {
      int splitIndex = authorizationHeader.indexOf(' ');
      jwt = authorizationHeader.substring(splitIndex + 1);
    }
    if (jwt == null || isTokenExpired(jwt)) {
      throw new InvalidCredentialsException(BusinessCode.MCU_INVALID_JWT, "Invalid jwt token");
    }
    return jwt;
  }
  
  public static Jwt parse(String token) {
    final Jwt jwt = Jwts.parser().setSigningKey(SECRET).parse(token);
    return jwt;
  }

  public static Long extractUserId(String token) {
    return Long.valueOf(extractClaim(token, Claims::getSubject));
  }

  private static boolean isTokenExpired(String token) {
    return extractClaim(token, Claims::getExpiration).before(new Date());
  }

  private static <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
    return claimsResolver.apply(claims);
  }
}
