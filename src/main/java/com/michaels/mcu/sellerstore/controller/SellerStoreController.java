package com.michaels.mcu.sellerstore.controller;

import com.michaels.mcu.security.annotation.RequireAuth;
import com.michaels.mcu.security.auth.AuthContext;
import com.michaels.mcu.sellerstore.entity.SellerAccountsPayableProfile;
import com.michaels.mcu.sellerstore.entity.SellerAccountsReceivableProfile;
import com.michaels.mcu.sellerstore.entity.SellerStore;
import com.michaels.mcu.sellerstore.payload.StoreRegisterRequest;
import com.michaels.mcu.sellerstore.service.SellerStoreService;
import com.michaels.mcu.sellerstore.utility.SellerStoreUtility;

import com.michaels.mcu.shared.common.BusinessCode;
import com.michaels.mcu.shared.response.RestResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/sellerstore")
@Tag(
        name = "Seller store controller",
        description = "Contains endpoints for store registration and onboarding")
public class SellerStoreController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SellerStoreController.class);

    private final SellerStoreService sellerStoreService;

    @Autowired
    public SellerStoreController(final SellerStoreService sellerStoreService) {
        this.sellerStoreService = sellerStoreService;
    }

    @Operation(
        security = @SecurityRequirement(name = "bearerAuth"),
        summary = "Check store name usability")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Store name is valid."
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Store name has been used."
                    )
            }
    )
    @GetMapping("/validStoreName")
    @RequireAuth
    public RestResponse validStoreName(@RequestParam final String storeName) {
        LOGGER.debug("SellerStoreController.validStoreName start");
        SellerStoreUtility.validStoreName(storeName, sellerStoreService, false);
        LOGGER.debug("SellerStoreController.validStoreName end");

        return RestResponse.ok("Store name is valid.");
    }

    @Operation(
        security = @SecurityRequirement(name = "bearerAuth"),
        summary = "Registration a store")
    @PostMapping("/registration")
    @RequireAuth
    public RestResponse createStore(@Valid @RequestBody StoreRegisterRequest store, final BindingResult bindingResult) {
        LOGGER.debug("SellerStoreController.createSellerStore start");
        AuthContext authContext = AuthContext.get();
        Long userId = authContext.getUserId();

        SellerStoreUtility.validCreateStoreParam(store, bindingResult);
        store.setUserId(userId);
        sellerStoreService.createStore(store);
        LOGGER.debug("SellerStoreController.createSellerStore end");

        return RestResponse.ok("operation success!");
    }

    @Operation(
        security = @SecurityRequirement(name = "bearerAuth"),
        summary = "Get seller store information")
    @GetMapping("/getSellerStoreInfo")
    @RequireAuth
    public RestResponse<Map<String, Object>> getSellerStoreInfo() {
        LOGGER.debug("SellerStoreController.getSellerStoreInfo start");
        AuthContext authContext = AuthContext.get();
        Long userId = authContext.getUserId();

        Map<String, Object> mapData = new HashMap<>();
        Optional<SellerStore> sellerStore = sellerStoreService.getStoreProfile(userId);
        if (sellerStore.isPresent()) {
            mapData.put("storeInfo", sellerStore.get());

            Optional<SellerAccountsReceivableProfile> sellerAccountsReceivableProfile = sellerStoreService.getReceivableProfile(userId,
                    sellerStore.get().getSellerStoreId());
            sellerAccountsReceivableProfile.ifPresent(accountsReceivableProfile -> mapData.put("accountsReceivableProfile", accountsReceivableProfile));

            Optional<SellerAccountsPayableProfile> sellerAccountsPayableProfile = sellerStoreService.getPayableProfile(userId,
                    sellerStore.get().getSellerStoreId());
            sellerAccountsPayableProfile.ifPresent(accountsPayableProfile -> mapData.put("accountsPayableProfile", accountsPayableProfile));
        }

        return new RestResponse<>(BusinessCode.MCU_200.getCode(), "success", mapData);
    }
    @Operation(
        security = @SecurityRequirement(name = "bearerAuth"),
        summary = "Get suggested store names")
    @GetMapping("/suggestSellerStoreName")
    @RequireAuth
    public RestResponse suggestSellerStoreName(@RequestParam final String typingStoreName) {
        LOGGER.debug("SellerStoreController.suggestSellerStoreName start");
        AuthContext authContext = AuthContext.get();
        Long userId = authContext.getUserId();

        Map<String, Object> mapData = new HashMap<>();
        List<String> sellerStoreNameList = new ArrayList<>();
        mapData.put("sellerStoreNameList", sellerStoreNameList);

        return new RestResponse<>(BusinessCode.MCU_200.getCode(), "success", mapData);
    }

//    @PostMapping(path = "/profile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
//    public void createProfile(StoreProfileRequest profile) {
//        SellerStoreUtility.validStoreProfile(profile, sellerStoreService);
//    }
}
