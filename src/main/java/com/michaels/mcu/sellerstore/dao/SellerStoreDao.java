package com.michaels.mcu.sellerstore.dao;

import com.mongodb.client.result.UpdateResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SellerStoreDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(SellerStoreDao.class);

    final MongoTemplate mongoTemplate;

    @Autowired
    public SellerStoreDao(final MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public void updateCustomerService(final Long sellerStoreId, final String customerService) {
    }

    @Transactional
    public long updateStateById(String docId, Long userId, Integer status) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(docId).and("user_id").is(userId));
        Update update = Update.update("status", status);
        UpdateResult updateResult = mongoTemplate.upsert(query, update, "seller_store");

        return updateResult.getMatchedCount();
    }
}
