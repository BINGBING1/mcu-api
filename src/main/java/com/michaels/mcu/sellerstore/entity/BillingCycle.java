package com.michaels.mcu.sellerstore.entity;

import java.util.HashMap;
import java.util.Map;

public enum BillingCycle {
  MONTH(0),
  HALF_YEAR(1),
  YEAR(2);

  private int value;

  BillingCycle(int value) {
    this.value = value;
  }

  private static Map<Integer, BillingCycle> mapping = new HashMap<>();

  static {
    for (BillingCycle e : BillingCycle.values()) {
      mapping.put(e.value, e);
    }
  }

  public static BillingCycle valueOf(int i) {
    return mapping.get(i);
  }
}
