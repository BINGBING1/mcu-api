package com.michaels.mcu.sellerstore.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
public class CustomerService {

    @Field(name = "is_phone_default_contact")
    private boolean isPhoneDefaultContact;

    @Field(name = "emails")
    private List<Email> emails;

    @Field(name = "phones")
    private List<Phone> phones;

    @Field(name = "fax")
    private String fax;
    //string?
    @Field(name = "address_id")
    private Long addressId;

    @Field(name = "privacy_notice")
    private String privacyNotice;
}
