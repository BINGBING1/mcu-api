package com.michaels.mcu.sellerstore.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class Email {
    @Field(name = "email_address")
    private String emailAddress;

    @Field(name = "service_hour")
    private ServiceHour serviceHour;
}
