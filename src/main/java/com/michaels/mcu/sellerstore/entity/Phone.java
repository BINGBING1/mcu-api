package com.michaels.mcu.sellerstore.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class Phone {
    @Field(name = "phone_number")
    private String phoneNumber;

    @Field(name = "service_hour")
    private ServiceHour serviceHour;
}
