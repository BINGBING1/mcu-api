package com.michaels.mcu.sellerstore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "seller_accounts_payable_profile")
public class SellerAccountsPayableProfile {
    @Id
    @Column(name = "seller_accounts_payable_profile_id")
    private Long entityId;

    @NotNull
    @Column(name = "store_id")
    private Long storeId;

    @NotNull
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "seller_plan_id")
    private Long sellerPlanId;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "billing_cycle")
    private BillingCycle billingCycle;

    @Column(name = "auto_billing")
    private Boolean autoBilling;

    @Column(name = "payment_method_id")
    private Long paymentMethodId;

    @Column(name = "billing_address_id")
    private Long billingAddressId;

    @Field(name = "created_time")
    @CreatedDate
    private LocalDateTime createdTime;

    @Field(name = "updated_time")
    @LastModifiedDate
    private LocalDateTime updatedTime;
}
