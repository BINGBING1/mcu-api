package com.michaels.mcu.sellerstore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.michaels.mcu.security.encryption.PIIConverter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "seller_accounts_receivable_profile")
public class SellerAccountsReceivableProfile {
    @Id
    @Column(name = "seller_accounts_receivable_profile_id")
    private Long entityId;

    @NotNull
    @Column(name = "store_id")
    private Long storeId;

    @NotNull
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "bank_country")
    private String bankCountry;

    @Column(name = "bank_account_type")
    private Byte bankAccountType;

    @Column(name = "full_name")
    private String fullName;

    @Convert(converter = PIIConverter.class)
    @Column(name = "encrypted_account_number")
    private String encryptedAccountNumber;

    @Column(name = "routing_number")
    private String routingNumber;

    @Column(name = "country_of_residence")
    private String countryOfResidence;

    @Column(name = "legal_name")
    private String legalName;

    @Column(name = "business_name")
    private String businessName;

    @Column(name = "legal_id_type")
    private String legalIdType;

    @Convert(converter = PIIConverter.class)
    @Column(name = "encrypted_date_of_birth")
    protected String encryptedDateOfBirth;

    @Convert(converter = PIIConverter.class)
    @Column(name = "encrypted_social_security_number")
    private String encryptedSocialSecurityNumber;

    @Column(name = "ein_number")
    private String einNumber;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "address_line1")
    private String addressLine1;

    @Column(name = "address_line2")
    private String addressLine2;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Field(name = "created_time")
    @CreatedDate
    private LocalDateTime createdTime;

    @Field(name = "updated_time")
    @LastModifiedDate
    private LocalDateTime updatedTime;
}
