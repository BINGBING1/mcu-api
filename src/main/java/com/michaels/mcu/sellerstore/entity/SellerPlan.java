package com.michaels.mcu.sellerstore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;

@Document(collection = "seller_plan")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SellerPlan {
    @Id
    private Long sellerPlanId;

    @Field("type")
    private String type;

    @Field("plan_name")
    private String planName;

    @Field("plan_price")
    private Double planPrice;

    @Field("description")
    private String description;
}
