package com.michaels.mcu.sellerstore.entity;

import java.util.HashMap;
import java.util.Map;

public enum SellerPreference {
  FULL_TIME(0),
  FULL_PART_TIME(1),
  PART_TIME(2),
  OTHER(3);
  private int value;
  SellerPreference (int value) {
    this.value = value;
  }
  private static Map<Integer, SellerPreference> mapping = new HashMap<>();

  static {
    for (SellerPreference e : SellerPreference.values()) {
      mapping.put(e.value, e);
    }
  }
  public static SellerPreference valueOf(int i) {
    return mapping.get(i);
  }
}
