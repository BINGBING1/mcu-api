package com.michaels.mcu.sellerstore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;
import org.hibernate.validator.constraints.URL;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Document(collection = "seller_store")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SellerStore {

    @Id
    private String _id;

    @Field(name = "seller_store_id")
    private Long sellerStoreId;

    @Indexed
    @Field(name = "user_id")
    @NotNull
    private Long userId;

    @Field(name = "seller_plan_id")
    private Long sellerPlanId;

    @Indexed(unique = true)
    @Field(name = "store_name")
    private String storeName;

    @Field(name = "display_name")
    private String displayName;

    @Field(name = "store_type")
    private int storeType;

    @Field(name = "store_logo")
    private String storeLogo;

    @Field(name = "store_dashboard_flag")
    private Boolean setupDashboardFlag;

    @Field(name = "start_marketplace_step")
    private Boolean startMarketplaceStep;

    @Field(name = "store_open_time_status")
    private Integer storeOpenTimeStatus;

    @URL
    @Field(name = "domain_name1")
    private String domainName1;

    @URL
    @Field(name = "domain_name2")
    private String domainName2;

    @Enumerated(EnumType.ORDINAL)
    @Field(name = "seller_preference")
    private SellerPreference sellerPreference;

    @Field(name = "seller_categories")
    private List<String> sellerCategories;//T array?

    @Field(name = "description")
    private String description;

    @Field(name = "deposit_frequence")
    private Integer depositFrequence;

    @Field(name = "last_payment_date")
    private Date lastPaymentDate;

    @Field(name = "opening_date")
    private Date openingDate;

    @Field(name = "end_date")
    private Date endDate;

    @Field(name = "national_id")
    private String nationalId;

    @Field(name = "national_id_pic_urls")
    private List<String> nationalIdPicUrls;

    @Field(name = "customer_service")
    private CustomerService customerService;

    @Field(name = "return_policy_option")
    private Integer returnPolicyOption;

    @Field(name = "return_policy_notice")
    private String returnPolicyNotice;

    @Field(name = "setting")
    private Setting setting;

    @Field(name = "status")
    private Integer status;

    @Field(name = "reject_reason")
    private String rejectReason;

    @Field(name = "tags")
    private List<String> tags;

    @Field(name = "quick_link_edit")
    private List<String> quickLinkEdit;

    @Field(name = "social_media_link")
    private String socialMediaLink;

    @Field(name = "processed_by")
    private Long processedBy;

    @Field(name = "created_time")
    @CreatedDate
    @Immutable
    private Date createdTime;

    @Field(name = "updated_time")
    @LastModifiedDate
    private Date updatedTime;
}
