package com.michaels.mcu.sellerstore.entity;

import java.util.HashMap;
import java.util.Map;

public enum SellerStoreCreatePhase {
    SAVED_STORE_INFO(0),
    SAVED_RECEVIABLE_INFO(1),
    SAVED_PAYABLE_INFO(2);

    private int value;
    SellerStoreCreatePhase (int value) {
        this.value = value;
    }
    private static Map<Integer, SellerStoreCreatePhase> mapping = new HashMap<>();

    static {
        for (SellerStoreCreatePhase e : SellerStoreCreatePhase.values()) {
            mapping.put(e.value, e);
        }
    }
    public static SellerStoreCreatePhase valueOf(int i) {
        return mapping.get(i);
    }
}
