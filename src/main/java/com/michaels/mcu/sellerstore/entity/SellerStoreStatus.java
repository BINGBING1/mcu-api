package com.michaels.mcu.sellerstore.entity;

import java.util.HashMap;
import java.util.Map;

public enum SellerStoreStatus {
  DRAFT(0),
  PENDING(1),
  APPROVED(2),
  REJECTED(3);

  private int value;
  SellerStoreStatus (int value) {
    this.value = value;
  }
  private static Map<Integer, SellerStoreStatus> mapping = new HashMap<>();

  static {
    for (SellerStoreStatus e : SellerStoreStatus.values()) {
      mapping.put(e.value, e);
    }
  }
  public static SellerStoreStatus valueOf(int i) {
    return mapping.get(i);
  }
}
