package com.michaels.mcu.sellerstore.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;
@Data
public class ServiceHour {
    @Field(name = "from")
    private String from;

    @Field(name = "to")
    private String to;

    @Field(name = "timezone")
    private int timezone;

    @Field(name = "weekdays")
    private String weekdays;
}
