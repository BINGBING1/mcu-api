package com.michaels.mcu.sellerstore.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;
@Data
public class Setting {

    @Field(name = "international_shipping")
    private boolean internationalShipping;

    @Field(name = "order_cancellations")
    private boolean orderCancellations;

    @Field(name = "order_returns")
    private boolean orderReturns;

    @Field(name = "order_replacements")
    private String orderReplacements;
}
