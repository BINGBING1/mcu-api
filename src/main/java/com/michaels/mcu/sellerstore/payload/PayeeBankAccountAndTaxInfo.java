package com.michaels.mcu.sellerstore.payload;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PayeeBankAccountAndTaxInfo {
    private Long entityId;
    private String fullName;
    private Byte bankAccountType;
    private String accountNumber;
    private String routingNumber;


    private String legalName;
    private LocalDate dateOfBirth;
    private String legalIdType;
    private String socialSecurityNumber;



    private String firstName;
    private String lastName;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String zipCode;
    private String phoneNumber;
}
