package com.michaels.mcu.sellerstore.payload;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PayerBillingInfo {
    private Long entityId;
    private Long sellerPlanId;
    private Integer billingCycle;
    private Boolean autoBilling;


    private String cardHolderName;
    private String cardNumber;
    private LocalDate expirationDate;
    private String cvv;


    private String firstName;
    private String lastName;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String zipCode;
    private String phoneNumber;
}
