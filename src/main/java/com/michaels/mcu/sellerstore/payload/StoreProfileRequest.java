package com.michaels.mcu.sellerstore.payload;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class StoreProfileRequest {
    private String displayName;
    private String description;
    private MultipartFile img;
}
