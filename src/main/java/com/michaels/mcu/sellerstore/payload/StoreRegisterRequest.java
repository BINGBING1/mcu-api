package com.michaels.mcu.sellerstore.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class StoreRegisterRequest {
    @NotBlank
    private String storeName;

    private Integer sellerPreference;
    private List<String> sellerCategories;

    private Long userId;

    private Long sellerStoreId;   //for editor

    @NotNull
    private Integer phaseNum;   //1, store info, 2, tax info, 3, billing info

    @NotNull
    private Integer storeType; //0: TPM, 1: FGM; If a third party or finished good marketplace
    // use payerBillingInfo instead of old one
    private PayerBillingInfo payerBillingInfo;

    private PayeeBankAccountAndTaxInfo payeeBankAccountAndTaxInfo;
}
