package com.michaels.mcu.sellerstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.michaels.mcu.sellerstore.entity.SellerAccountsPayableProfile;

import java.util.Optional;

@Repository
public interface SellerAccountsPayableProfileRepository extends JpaRepository<SellerAccountsPayableProfile, Long> {
    Optional<SellerAccountsPayableProfile> findByUserIdAndStoreId(Long userId, Long sellerStoreId);

    Optional<SellerAccountsPayableProfile> findByUserId(Long userId);
}
