package com.michaels.mcu.sellerstore.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.michaels.mcu.sellerstore.entity.SellerStore;

import java.util.Optional;

@Repository
public interface SellerStoreRepository extends MongoRepository<SellerStore, Long> {
    Optional<SellerStore> findByStoreNameIgnoreCase(String storeName);
    Optional<SellerStore> findByDisplayNameIgnoreCase(String name);

    Optional<SellerStore> findByUserId(Long userId);
}
