package com.michaels.mcu.sellerstore.service;


import com.michaels.mcu.sellerstore.entity.SellerAccountsPayableProfile;
import com.michaels.mcu.sellerstore.entity.SellerAccountsReceivableProfile;
import com.michaels.mcu.sellerstore.entity.SellerStore;
import com.michaels.mcu.sellerstore.payload.StoreRegisterRequest;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface SellerStoreService {
    Optional<SellerStore> findStoreByStoreName(String name);
    Optional<SellerStore> findStoreByDisplayName(String name);
    Boolean createStore(StoreRegisterRequest storeProfile);
    boolean validStoreTextContent(String content);

    Optional<SellerAccountsReceivableProfile> getReceivableProfile(Long userId, Long sellerStoreId);
    Optional<SellerAccountsPayableProfile> getPayableProfile(Long userId, Long sellerStoreId);
    Optional<SellerStore> getStoreProfile(Long userId);
}
