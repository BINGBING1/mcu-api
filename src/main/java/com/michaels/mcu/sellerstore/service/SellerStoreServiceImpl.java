package com.michaels.mcu.sellerstore.service;

import com.michaels.mcu.sellerstore.dao.SellerStoreDao;
import com.michaels.mcu.sellerstore.entity.*;
import com.michaels.mcu.sellerstore.payload.StoreRegisterRequest;
import com.michaels.mcu.sellerstore.payload.PayeeBankAccountAndTaxInfo;
import com.michaels.mcu.sellerstore.repository.SellerAccountsPayableProfileRepository;
import com.michaels.mcu.sellerstore.repository.SellerAccountsReceivableProfileRepository;
import com.michaels.mcu.sellerstore.repository.SellerStoreRepository;
import com.michaels.mcu.sellerstore.utility.SellerStoreUtility;
import com.michaels.mcu.shared.common.BusinessCode;
import com.michaels.mcu.shared.exception.BadRequestException;
import com.michaels.mcu.shared.exception.ServiceUnavailableException;
import com.michaels.mcu.shared.generator.util.IdGeneratorUtils;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class SellerStoreServiceImpl implements SellerStoreService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SellerStoreServiceImpl.class);
    private final SellerStoreRepository sellerStoreRepository;
    private final SellerAccountsPayableProfileRepository payableProfileRepository;
    private final SellerAccountsReceivableProfileRepository receivableProfileRepository;
    private final RestTemplate restTemplate;

    @Autowired
    private SellerStoreDao sellerStoreDao;

    @Value("${michaels.service.cmsUrl}")
    private String cmsUrl;

    @Value("${michaels.service.domain}")
    private String domain;

    @Value("${michaels.service.subDomain}")
    private String subDomain;

    @Autowired
    public SellerStoreServiceImpl(
            final SellerStoreRepository sellerStoreRepository,
            final SellerAccountsPayableProfileRepository payableProfileRepository,
            final SellerAccountsReceivableProfileRepository receivableProfileRepository,
            final RestTemplate restTemplate) {
        this.sellerStoreRepository = sellerStoreRepository;
        this.payableProfileRepository = payableProfileRepository;
        this.receivableProfileRepository = receivableProfileRepository;
        this.restTemplate = restTemplate;
    }

    @Override
    public Optional<SellerStore> findStoreByStoreName(String storeName) {
        LOGGER.debug("SellerStoreService.findStoreByStoreName start");
        Optional<SellerStore> store = sellerStoreRepository.findByStoreNameIgnoreCase(storeName);
        LOGGER.debug("SellerStoreService.findStoreByStoreName end");
        return store;
    }

    @Override
    public Optional<SellerStore> findStoreByDisplayName(String name) {
        LOGGER.debug("SellerStoreService.findStoreByStoreName start");
        Optional<SellerStore> store = sellerStoreRepository.findByDisplayNameIgnoreCase(name);
        LOGGER.debug("SellerStoreService.findStoreByStoreName end");
        return store;
    }

    @Override
    public Boolean createStore(StoreRegisterRequest storeProfile) {
        LOGGER.debug("SellerStoreService.createStore start");

        Optional<SellerStore> sellerStore = getStoreProfile(storeProfile.getUserId());
        if (sellerStore.isPresent()
                && !sellerStore.get().getStatus().equals(SellerStoreStatus.DRAFT.ordinal())) {
            LOGGER.error("cann't modify, store info is in draft, request:{}", storeProfile);   //TODO don't displlay all info
            throw new BadRequestException(BusinessCode.MCU_STORE_CANNOT_MODIFY, "store info cann't modify");
        }

        if (SellerStoreCreatePhase.SAVED_STORE_INFO.equals(SellerStoreCreatePhase.valueOf(storeProfile.getPhaseNum()))) {
            saveStoreProfile(storeProfile, sellerStore);
        } else if (SellerStoreCreatePhase.SAVED_RECEVIABLE_INFO.equals(SellerStoreCreatePhase.valueOf(storeProfile.getPhaseNum()))) {
            if (sellerStore.isEmpty()) {
                LOGGER.error("phase:SAVED_RECEIVABLE_INFO, bad request, request:{}", storeProfile.toString());   //TODO don't displlay all info
                throw new BadRequestException(BusinessCode.MCU_USR_HAVE_NOT_STORE_PROFILE, "user doesn't have store profile");
            }

            saveReceivableProfile(storeProfile, sellerStore.get());
        } else if (SellerStoreCreatePhase.SAVED_PAYABLE_INFO.equals(SellerStoreCreatePhase.valueOf(storeProfile.getPhaseNum()))) {
            if (sellerStore.isEmpty()) {
                LOGGER.error("phase:SAVED_PAYABLE_INFO  bad request, request:{}", storeProfile.toString());   //TODO don't displlay all info
                throw new BadRequestException(BusinessCode.MCU_USR_HAVE_NOT_STORE_PROFILE, "user doesn't have store profile");
            }

            savePayableProfile(storeProfile, sellerStore.get());
            sellerStoreDao.updateStateById(sellerStore.get().get_id(), sellerStore.get().getUserId(),
                    SellerStoreStatus.APPROVED.ordinal());
        } else {
            LOGGER.error("error phase type, phase:{}", storeProfile.getPhaseNum());
            throw new BadRequestException(BusinessCode.MCU_BAD_PHASE, "error phase type");
        }
        LOGGER.debug("SellerStoreService.createStore start");
        return true;
    }

    @Override
    public boolean validStoreTextContent(String content) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        //Temporary JSON, CMS will provide new API only for validation purpose
        final JSONObject json = new JSONObject();
        try {
            json.put("values", Collections.singletonList(content));
            json.put("clientId", "string");
            json.put("sourceId", "string");
            json.put("textType", "STORE_NAME");
            json.put("needScreened", true);
        } catch (Exception e) {
            LOGGER.info("Remote API service is not available", e);
            throw new ServiceUnavailableException(BusinessCode.MCU_INTERNAL_ERROR,
                    "Remote API service is not available");
        }

        HttpEntity<String> request = new HttpEntity<>(json.toString(), headers);
        ResponseEntity<String> resp;

        try {
            resp = restTemplate.exchange(cmsUrl, HttpMethod.POST, request, String.class);
        } catch (HttpClientErrorException e) {
            throw new ServiceUnavailableException(BusinessCode.MCU_INTERNAL_ERROR,
                    "Remote API service is not available");
        }

        //Temporary use for content validation. CMS will provide new API only for validation purpose
        return resp.getBody() != null && resp.getBody().contains("INAPPROPRIATE");
    }

    private void saveStoreProfile(StoreRegisterRequest storeProfile, Optional<SellerStore> oldSellerStore) {
        LOGGER.debug("SellerStoreService.createStoreProfile start");
        String storeName = storeProfile.getStoreName();

        //parse domain name
        StringBuilder builder = new StringBuilder(storeName);
        String domainName1 = builder.append(subDomain).toString();
        builder = new StringBuilder(storeName);
        String domainName2 = builder.insert(0, domain + "/").toString();

        //parse categories to ArrayList<String>
        List<String> categories = new ArrayList<>();
        Object obj =  storeProfile.getSellerCategories();
        if(obj instanceof ArrayList<?>) {
            for(Object o : (List<?>) obj) {
                categories.add((String) o);
            }
        }

        //parse preference to int
        SellerPreference storePreference = SellerPreference.valueOf(storeProfile.getSellerPreference());

        //create as draft
        int status = SellerStoreStatus.DRAFT.ordinal();

        //create or editor
        if (oldSellerStore.isPresent()) {
            SellerStore newSellerStore = oldSellerStore.get();
            if (!newSellerStore.getStoreName().equals(storeName)) {
                //valid store name
                SellerStoreUtility.validStoreName(storeProfile.getStoreName(), this, false);
            }
            newSellerStore.setStoreName(storeName);
            newSellerStore.setDomainName1(domainName1);
            newSellerStore.setDomainName2(domainName2);
            newSellerStore.setSellerPreference(storePreference);
            newSellerStore.setSellerCategories(categories);
            newSellerStore.setDescription("");
            newSellerStore.setStatus(status);
            newSellerStore.setUpdatedTime(new Date());

            sellerStoreRepository.save(newSellerStore);
        } else {
            //valid store name
            SellerStoreUtility.validStoreName(storeProfile.getStoreName(), this, false);

            SellerStore sellerStore = SellerStore.builder()
                    .sellerStoreId(IdGeneratorUtils.getUID())
                    .userId(storeProfile.getUserId())
                    //.sellerPlanId(sellerPlanRepository.findByPlanType(storeProfile.getBillingInfo().get("sellerPlan")).getId())
                    .storeName(storeName)
                    .storeType(storeProfile.getStoreType())
                    .domainName1(domainName1)
                    .domainName2(domainName2)
                    .sellerPreference(storePreference)
                    .sellerCategories(categories)
                    .description("")
                    //.depositFrequence()
                    //.lastPaymentDate()
                    .status(status)
                    .createdTime(new Date())
                    .updatedTime(new Date())
                    .build();
            sellerStoreRepository.save(sellerStore);
        }

        LOGGER.debug("SellerStoreService.createStoreProfile end");
        return;
    }

    @Override
    public Optional<SellerStore> getStoreProfile(Long userId) {
        return sellerStoreRepository.findByUserId(userId);
    }

    private void savePayableProfile(StoreRegisterRequest storeProfile, SellerStore sellerStore) {
        LOGGER.debug("SellerStoreService.createPayableProfile end");
        //parse billing cycle
        BillingCycle billingCycle = BillingCycle.valueOf(
                storeProfile.getPayerBillingInfo().getBillingCycle());
        //parse auto billing
        boolean autoBilling = storeProfile.getPayerBillingInfo().getAutoBilling();

        //create or editor
        Optional<SellerAccountsPayableProfile> oldSellerAccountsPayableProfile = payableProfileRepository.findByUserId(storeProfile.getUserId());
        if (oldSellerAccountsPayableProfile.isPresent()) {
            SellerAccountsPayableProfile newSellerAccountsPayableProfile = oldSellerAccountsPayableProfile.get();
            newSellerAccountsPayableProfile.setSellerPlanId(storeProfile.getPayerBillingInfo().getSellerPlanId());   //TODO for test
            newSellerAccountsPayableProfile.setBillingCycle(billingCycle);
            newSellerAccountsPayableProfile.setAutoBilling(autoBilling);
            newSellerAccountsPayableProfile.setPaymentMethodId(1431241L);  //TODO for test
            newSellerAccountsPayableProfile.setBillingAddressId(431413241L);  //TODO for test
            newSellerAccountsPayableProfile.setUpdatedTime(LocalDateTime.now());
            payableProfileRepository.save(newSellerAccountsPayableProfile);
        } else {
            SellerAccountsPayableProfile payableProfile = SellerAccountsPayableProfile.builder()
                    .entityId(IdGeneratorUtils.getUID())
                    .storeId(sellerStore.getSellerStoreId())
                    .userId(storeProfile.getUserId())
                    .sellerPlanId(storeProfile.getPayerBillingInfo().getSellerPlanId())   //TODO for test
                    .billingCycle(billingCycle)
                    .autoBilling(autoBilling)
                    .paymentMethodId(1431241L)   //TODO for test
                    .billingAddressId(431413241L)  //TODO for test
                    .createdTime(LocalDateTime.now())
                    .updatedTime(LocalDateTime.now())
                    .build();
            payableProfileRepository.save(payableProfile);
        }

        LOGGER.debug("SellerStoreService.createPayableProfile end");
    }

    @Override
    public Optional<SellerAccountsPayableProfile> getPayableProfile(Long userId, Long sellerStoreId) {
        return payableProfileRepository.findByUserIdAndStoreId(userId, sellerStoreId);
    }

    private void saveReceivableProfile(StoreRegisterRequest storeProfile, SellerStore sellerStore) {
        LOGGER.debug("SellerStoreService.createReceivableProfile end");
        PayeeBankAccountAndTaxInfo payeeBankAccountAndTaxInfo = storeProfile.getPayeeBankAccountAndTaxInfo();
        String fullName = payeeBankAccountAndTaxInfo.getFullName();
        String encryptedCardNumber = payeeBankAccountAndTaxInfo.getAccountNumber();
        String encryptedSSN = payeeBankAccountAndTaxInfo.getSocialSecurityNumber();

        //TODO, need implement encrypt

        //create or editor
        Optional<SellerAccountsReceivableProfile> oldSellerAccountsReceivableProfile = receivableProfileRepository.findByUserId(storeProfile.getUserId());
        if (oldSellerAccountsReceivableProfile.isPresent()) {
            SellerAccountsReceivableProfile newSellerAccountsReceivableProfile = oldSellerAccountsReceivableProfile.get();
            newSellerAccountsReceivableProfile.setBankAccountType(storeProfile.getPayeeBankAccountAndTaxInfo().getBankAccountType());
            newSellerAccountsReceivableProfile.setFullName(fullName);
            newSellerAccountsReceivableProfile.setEncryptedAccountNumber(encryptedCardNumber);
            newSellerAccountsReceivableProfile.setRoutingNumber(payeeBankAccountAndTaxInfo.getRoutingNumber());
            newSellerAccountsReceivableProfile.setLegalName(payeeBankAccountAndTaxInfo.getLegalName());
            newSellerAccountsReceivableProfile.setLegalIdType(payeeBankAccountAndTaxInfo.getLegalIdType());
            newSellerAccountsReceivableProfile.setEncryptedDateOfBirth(payeeBankAccountAndTaxInfo.getDateOfBirth().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            newSellerAccountsReceivableProfile.setEncryptedSocialSecurityNumber(encryptedSSN);
            newSellerAccountsReceivableProfile.setAddressLine1(payeeBankAccountAndTaxInfo.getAddressLine1());
            newSellerAccountsReceivableProfile.setAddressLine2(payeeBankAccountAndTaxInfo.getAddressLine2());
            newSellerAccountsReceivableProfile.setZipCode(payeeBankAccountAndTaxInfo.getZipCode());
            newSellerAccountsReceivableProfile.setPhoneNumber(payeeBankAccountAndTaxInfo.getPhoneNumber());
            newSellerAccountsReceivableProfile.setUpdatedTime(LocalDateTime.now());
            receivableProfileRepository.save(newSellerAccountsReceivableProfile);
        } else {
            SellerAccountsReceivableProfile receivableProfile = SellerAccountsReceivableProfile.builder()
                    .entityId(IdGeneratorUtils.getUID())
                    .storeId(sellerStore.getSellerStoreId())
                    .userId(storeProfile.getUserId())
                    .bankAccountType(storeProfile.getPayeeBankAccountAndTaxInfo().getBankAccountType())
                    .fullName(fullName)
                    .encryptedAccountNumber(encryptedCardNumber)
                    .routingNumber(payeeBankAccountAndTaxInfo.getRoutingNumber())
                    .legalName(payeeBankAccountAndTaxInfo.getLegalName())
                    .legalIdType(payeeBankAccountAndTaxInfo.getLegalIdType())
                    .encryptedDateOfBirth(payeeBankAccountAndTaxInfo.getDateOfBirth().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                    .encryptedSocialSecurityNumber(encryptedSSN)
                    .addressLine1(payeeBankAccountAndTaxInfo.getAddressLine1())
                    .addressLine2(payeeBankAccountAndTaxInfo.getAddressLine2())
                    .zipCode(payeeBankAccountAndTaxInfo.getZipCode())
                    .phoneNumber(payeeBankAccountAndTaxInfo.getPhoneNumber())
                    .createdTime(LocalDateTime.now())
                    .updatedTime(LocalDateTime.now())
                    .build();
            receivableProfileRepository.save(receivableProfile);
        }

        LOGGER.debug("SellerStoreService.createReceivableProfile end");
    }

    @Override
    public Optional<SellerAccountsReceivableProfile> getReceivableProfile(Long userId, Long sellerStoreId) {
        return receivableProfileRepository.findByUserIdAndStoreId(userId, sellerStoreId);
    }
}
