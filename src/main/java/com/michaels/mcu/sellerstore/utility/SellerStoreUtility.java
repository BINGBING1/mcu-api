package com.michaels.mcu.sellerstore.utility;

import com.michaels.mcu.sellerstore.entity.SellerStoreCreatePhase;
import com.michaels.mcu.sellerstore.payload.StoreProfileRequest;
import com.michaels.mcu.sellerstore.payload.StoreRegisterRequest;
import com.michaels.mcu.sellerstore.service.SellerStoreService;
import com.michaels.mcu.shared.common.BusinessCode;
import com.michaels.mcu.shared.exception.BadRequestException;
import com.michaels.mcu.shared.exception.DuplicateException;
import com.michaels.mcu.shared.exception.NotFoundException;
import com.michaels.mcu.shared.exception.RequestArgumentNotValidException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;

import java.util.HashMap;
import java.util.Map;

public final class SellerStoreUtility {
    private static final Logger LOGGER = LoggerFactory.getLogger(SellerStoreUtility.class);

    //display name and store name validation is same;
    public static void validStoreName(String storeName, SellerStoreService sellerstoreService, boolean isDisplayName) {
        LOGGER.debug("validStoreName start");
        if (storeName == null || storeName.isEmpty() || storeName.isBlank()) {
            throw new NotFoundException(BusinessCode.MCU_STORE_NAME_NEEDED, "Store name is not provided");
        }

        if (!isDisplayName) {
            if (sellerstoreService.findStoreByStoreName(storeName).isPresent()) {
                throw new DuplicateException(BusinessCode.MCU_STORE_NAME_USED, "Store name has been used");
            }
        } else {
            if (sellerstoreService.findStoreByDisplayName(storeName).isPresent()) {
                throw new DuplicateException(BusinessCode.MCU_DISPLAY_USED, "Display name has been used");
            }
        }

        if (sellerstoreService.validStoreTextContent(storeName)) {
            throw new BadRequestException(BusinessCode.MCU_INAPPROPRIATE, "Store name contains inappropriate content");
        }

        LOGGER.debug("validStoreName end");
    }

    public static void validCreateStoreParam(StoreRegisterRequest storeProfile, BindingResult bindingResult) {
        //Not null validation for store profile
        if(SellerStoreCreatePhase.SAVED_STORE_INFO.equals(SellerStoreCreatePhase.valueOf(storeProfile.getPhaseNum()))
            && storeProfile.getSellerPreference() == null) {
            bindingResult.rejectValue("storePref", "Store preference not provided","The store preference is not valid");
        }

        if(SellerStoreCreatePhase.SAVED_RECEVIABLE_INFO.equals(SellerStoreCreatePhase.valueOf(storeProfile.getPhaseNum()))
                && storeProfile.getPayerBillingInfo() == null){
            bindingResult.rejectValue("billingInfo", "Billing information not provided","Billing information is not valid");
        }

        if(SellerStoreCreatePhase.SAVED_PAYABLE_INFO.equals(SellerStoreCreatePhase.valueOf(storeProfile.getPhaseNum()))
            && storeProfile.getPayeeBankAccountAndTaxInfo() == null){
            bindingResult.rejectValue("taxInfo", "Tax information not provided","Tax information is not valid");
        }

        final Map<String, String> errorMap = SellerStoreUtility.getErrorMap(bindingResult);
        if (!errorMap.isEmpty()) {
            LOGGER.debug("validation error# {}", errorMap.size());
            throw new RequestArgumentNotValidException(BusinessCode.MCU_ARGUMENT_NOT_VALID, errorMap);
        }
    }

    public static void validStoreProfile(StoreProfileRequest profile, SellerStoreService sellerstoreService) {
        String displayName = profile.getDisplayName();
        String description = profile.getDescription();
        validStoreName(displayName, sellerstoreService, true);

        if(sellerstoreService.validStoreTextContent(description)) {
            throw new BadRequestException(BusinessCode.MCU_INAPPROPRIATE, "Description contains inappropriate content");
        }
    }

    public static Map<String, String> getErrorMap(BindingResult result) {
        Map<String, String> errorMap = new HashMap<>();
        if (result.hasErrors()) {
            result
                    .getFieldErrors()
                    .forEach(error -> errorMap.put(error.getField(), error.getDefaultMessage()));
        }
        return errorMap;
    }
}
