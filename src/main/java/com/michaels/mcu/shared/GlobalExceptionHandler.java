package com.michaels.mcu.shared;

import java.util.stream.Collectors;

import com.michaels.mcu.shared.exception.MikException;
import com.michaels.mcu.shared.response.RestResponse;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValidException(final MethodArgumentNotValidException ex) {
        String message = ex.getBindingResult().getAllErrors().stream()
            .map(DefaultMessageSourceResolvable::getDefaultMessage)
            .collect(Collectors.joining());
        RestResponse messageResponse = RestResponse.fail(message);
        return new ResponseEntity<>(messageResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler(MikException.class)
    public ResponseEntity<Object> handleMikException(final MikException ex) {
        RestResponse messageResponse = RestResponse.fail(ex);
        return new ResponseEntity<>(messageResponse, new HttpHeaders(), ex.getStatus());
    }
}
