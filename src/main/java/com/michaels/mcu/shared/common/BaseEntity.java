package com.michaels.mcu.shared.common;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.michaels.mcu.shared.generator.util.IdGeneratorUtils;

public class BaseEntity implements Serializable {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using=ToStringSerializer.class)  
    protected Long id = IdGeneratorUtils.getUID();

    protected LocalDateTime createdTime;
    
    protected Long createdBy;

    protected LocalDateTime updatedTime;
    
    protected Long updatedBy;
    
    public BaseEntity() {
      super();
    }

    public BaseEntity(Long id, LocalDateTime createdTime, Long createdBy, LocalDateTime updatedTime, Long updatedBy) {
      super();
      this.id = id==null?this.id:id;
      this.createdTime = createdTime;
      this.createdBy = createdBy;
      this.updatedTime = updatedTime;
      this.updatedBy = updatedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(LocalDateTime updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }
    
}
