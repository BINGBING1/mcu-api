package com.michaels.mcu.shared.common;

import org.springframework.data.couchbase.core.mapping.Field;

public class CouchBaseEntity {
  
  @Field
  protected String entityType;

  
  public CouchBaseEntity() {
    super();
  }

  public CouchBaseEntity(String entityType) {
    super();
    this.entityType = entityType;
  }

  public String getType() {
    return entityType;
  }

  public void setType(String type) {
    this.entityType = type;
  }
  
  
}
