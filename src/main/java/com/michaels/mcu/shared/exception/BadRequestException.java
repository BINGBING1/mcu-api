package com.michaels.mcu.shared.exception;

import com.michaels.mcu.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class BadRequestException extends MikException {
    public BadRequestException(BusinessCode bc, String message) {
        super(HttpStatus.BAD_REQUEST, bc, message);
    }
}
