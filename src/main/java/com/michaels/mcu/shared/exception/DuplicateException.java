package com.michaels.mcu.shared.exception;

import com.michaels.mcu.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class DuplicateException extends MikException {
    public DuplicateException(BusinessCode bc, String message) {
        super(HttpStatus.CONFLICT, bc, message);
    }
}
