package com.michaels.mcu.shared.exception;

import com.michaels.mcu.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class InvalidCredentialsException extends MikException {

  public InvalidCredentialsException(BusinessCode bc, String message) {
    super(HttpStatus.UNAUTHORIZED, bc, message);
  }
}
