package com.michaels.mcu.shared.exception;

import com.michaels.mcu.shared.common.BusinessCode;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
public class MikException extends ResponseStatusException {

    private BusinessCode bc;

    public MikException(HttpStatus status, BusinessCode bc, String message) {
        super(status, message);
        this.bc = bc;
    }

    public MikException(String message) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, message);
        this.bc = BusinessCode.MCU_INTERNAL_ERROR;
    }

    public MikException(String message, Throwable cause) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, message, cause);
        this.bc = BusinessCode.MCU_INTERNAL_ERROR;
    }

    public String getCode(){
        return this.bc.getCode();
    }
}
