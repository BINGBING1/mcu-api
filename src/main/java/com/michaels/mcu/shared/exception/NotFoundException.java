package com.michaels.mcu.shared.exception;

import com.michaels.mcu.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class NotFoundException extends MikException {
  public NotFoundException(BusinessCode bc, String message) {
    super(HttpStatus.NOT_FOUND, bc, message);
  }
}
