package com.michaels.mcu.shared.exception;

import com.michaels.mcu.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * An exception when arguments are not valid. Map<String, String> errors = new HashMap<>();
 * errors.put("arg1", "arg1's description"); errors.put("arg2", "arg2's description"); message
 * output will be like: arg1- arg1's description;arg2- arg2's description
 */
public class RequestArgumentNotValidException extends MikException {

  public RequestArgumentNotValidException(BusinessCode bc, Map<String, String> errors) {
    super(
        HttpStatus.BAD_REQUEST, bc,
            errors.keySet().stream()
            .map(key -> key + "- " + errors.get(key))
            .collect(Collectors.joining(";", "", "")));
  }
}
