package com.michaels.mcu.shared.exception;

import com.michaels.mcu.shared.common.BusinessCode;
import org.springframework.http.HttpStatus;

public class ServiceUnavailableException extends MikException {

  public ServiceUnavailableException(BusinessCode bc, String message) {
    super(HttpStatus.SERVICE_UNAVAILABLE, bc, message);
  }
}
