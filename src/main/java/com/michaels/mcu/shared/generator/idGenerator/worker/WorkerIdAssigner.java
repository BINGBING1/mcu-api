package com.michaels.mcu.shared.generator.idGenerator.worker;

public interface WorkerIdAssigner {

  /**
   * Assign worker id for
   *
   * @return assigned worker id
   */
  long assignWorkerId(long max);
}
