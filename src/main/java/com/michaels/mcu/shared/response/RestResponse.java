package com.michaels.mcu.shared.response;

import com.michaels.mcu.shared.common.BusinessCode;
import com.michaels.mcu.shared.exception.MikException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RestResponse<T> {
    private static final String SUCCESS_MESSAGE = "Succeeded";
    private static final String FAIL_MESSAGE = "Failed";

    private static final String CODE_OK = BusinessCode.MCU_200.getCode();
    private static final String CODE_ERROR = "Error";

    private String code;
    private String message;
    private T data;
    public RestResponse(String message, T data){
        this.code = CODE_OK;
        this.message = message;
        this.data = data;
    }
    public RestResponse(T data){
        this.code = CODE_OK;
        this.message = SUCCESS_MESSAGE;
        this.data = data;
    }

    public static RestResponse<Object> ok() {
        RestResponse<Object> okResponse = new RestResponse<Object>(CODE_OK, SUCCESS_MESSAGE, null);
        return okResponse;
    }

    public static RestResponse<Object> ok(String message) {
        RestResponse<Object> okResponse = new RestResponse<Object>(CODE_OK, message, null);
        return okResponse;
    }

    public static RestResponse<Object> fail(String message) {
        RestResponse<Object> okResponse = new RestResponse<Object>(CODE_ERROR, message, null);
        return okResponse;
    }

    public static RestResponse<Object> fail(MikException ex) {
        RestResponse<Object> okResponse = new RestResponse<Object>(ex.getCode(), ex.getMessage(), null);
        return okResponse;
    }
}
