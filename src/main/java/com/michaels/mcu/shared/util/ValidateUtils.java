package com.michaels.mcu.shared.util;

import com.michaels.mcu.shared.generator.idGenerator.utils.DateUtils;

import java.util.Date;

public class ValidateUtils {
    private static final int SIGNIN_QUALIFIED_AGE = 13;
    public static boolean validatePassword(String password){
        return password.matches("(.{6,14})")&&password.matches("(.*[A-Z]+.*)")&&password.matches("(.*[a-z]+.*)")&&password.matches("(.*[0-9]+.*)");
    }

    public static boolean validateBirthday(String birthday){
        if(StringUtils.isEmpty(birthday)){
        }else {
            try{
                Date date = DateUtils.parseDate(birthday, "MM/dd/YYYY");
                if(date.compareTo(DateUtils.addYears(new Date(), -SIGNIN_QUALIFIED_AGE)) > 0){
                    return false;
                }

            }catch (Exception e){
                return false;
            }
        }
        return true;
    }
}
