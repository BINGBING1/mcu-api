package com.michaels.mcu.shared.validation;

import com.michaels.mcu.shared.util.StringUtils;

import javax.validation.*;
import java.lang.annotation.*;

/**
 *  apply this to request entity validate phone
 */
@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER,ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {ValidatePhone.PhoneValidator.class})
public @interface ValidatePhone {
  String message() default "Phone number is not valid";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
  class PhoneValidator implements ConstraintValidator<ValidatePhone, String> {

    @Override
    public boolean isValid(String phone, ConstraintValidatorContext constraintValidatorContext) {
      if(!StringUtils.isEmpty(phone) && !phone.matches("\\d{10}$")){
        return false;
      }
      return true;
    }
  }
}
