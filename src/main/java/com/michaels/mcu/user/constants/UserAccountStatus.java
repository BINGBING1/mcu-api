package com.michaels.mcu.user.constants;

public enum UserAccountStatus {
  INACTIVE(0), 
  ACTIVE(1), 
  LOCKED(2), 
  DELETED(3);

  private final int value;

  private UserAccountStatus(int value) {
    this.value = value;
  }

  public static UserAccountStatus fromValue(int value) {
    if(value==INACTIVE.getValue()) {
      return INACTIVE;
    }
    if(value==ACTIVE.getValue()) {
      return ACTIVE;
    }
    if(value==LOCKED.getValue()) {
      return LOCKED;
    }
    if(value==DELETED.getValue()) {
      return DELETED;
    }
    return null;
  }

  public int getValue() {
    return value;
  }
}
