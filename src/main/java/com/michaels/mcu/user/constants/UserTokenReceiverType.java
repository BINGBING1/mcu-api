package com.michaels.mcu.user.constants;

public enum UserTokenReceiverType {
  EMAIL(0),
  PHONE(1);
  
  private final int value;

  public static UserTokenReceiverType fromValue(int value) {
    if(value==EMAIL.getValue()) {
      return EMAIL;
    }
    if(value==PHONE.getValue()) {
      return PHONE;
    }
    return null;
  }

  private UserTokenReceiverType(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

}
