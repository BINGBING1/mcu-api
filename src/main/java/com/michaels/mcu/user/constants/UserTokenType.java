package com.michaels.mcu.user.constants;

public enum UserTokenType {
  CONFIRM_EMAIL(0, 6L * 60L * 1000L), 
  RESET_PASSWORD(1, 6L * 60L * 1000L),
  TWO_FACTOR_AUTHENTICATION(2, 5L * 60L * 1000L), 
  LINK_REWARD(3, 10L * 60L * 1000L);

  private int value;

  private UserTokenType(int value, long expirationTime) {
    this.value = value;
    this.expirationTime = expirationTime;
  }
  
  public static UserTokenType fromValue(int value) {
    if(value==CONFIRM_EMAIL.getValue()) {
      return CONFIRM_EMAIL;
    }
    if(value==RESET_PASSWORD.getValue()) {
      return RESET_PASSWORD;
    }
    if(value==TWO_FACTOR_AUTHENTICATION.getValue()) {
      return TWO_FACTOR_AUTHENTICATION;
    }
    if(value==LINK_REWARD.getValue()) {
      return LINK_REWARD;
    }
    return null;
  }

  private final long expirationTime; // millisecond

  public int getValue() {
    return value;
  }

  public long getExpirationTime() {
    return expirationTime;
  }
}
