package com.michaels.mcu.user.controller;

import java.util.List;

import javax.validation.Valid;

import com.michaels.mcu.shared.response.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.michaels.mcu.security.annotation.RequireAuth;
import com.michaels.mcu.user.entity.UserAddress;
import com.michaels.mcu.user.payload.request.AddressChangeRequest;
import com.michaels.mcu.user.payload.request.DefaultAddressChangeRequest;
import com.michaels.mcu.user.service.UserAddressService;
import com.michaels.mcu.user.service.UserService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@CrossOrigin
@RestController
@RequestMapping("/user/address")
@Tag(
    name = "User Address Controller",
    description = "Contains endpoints for creating/updating/deleting a user address")
@RequireAuth
public class UserAddressController {
  
  private final UserAddressService userAddressService;
  private final UserService userService;

  @Autowired
  public UserAddressController(
      final UserAddressService userAddressService, UserService userService) {
    this.userAddressService = userAddressService;
    this.userService = userService;
  }

  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Get user address list")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Returned user address list",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserAddress.class))),
        @ApiResponse(
            responseCode = "401",
            description = "User is not authenticated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
      })
  @GetMapping()
  public RestResponse<List<UserAddress>> getAddressList() {
    
    List<UserAddress> userAddresses =  userAddressService.getAddressList();
    
    return new RestResponse<List<UserAddress>>("Returned user address list", userAddresses);
  }

  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Add an address to authenticated user")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Returned added address",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserAddress.class))),
        @ApiResponse(
            responseCode = "401",
            description = "User is not authenticated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
      })
  @PostMapping()
  public RestResponse<UserAddress> addAddress(
      @Valid @RequestBody AddressChangeRequest request) {
    return new RestResponse<>(userAddressService.createAddress(request));
  }

  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"), 
      summary = "Update an address")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Returned updated address",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserAddress.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Address was not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PatchMapping("/{userAddressId}")
  public RestResponse<UserAddress> updateAddress(
      @Valid @RequestBody AddressChangeRequest request, @PathVariable Long userAddressId) {
    return new RestResponse<>(userAddressService.updateAddress(request, userAddressId));
  }
  
  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Set default shipping address for the authenticated user")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Set default shipping address successfully",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Address was not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
      })
  @PostMapping("/set-default-shipping-address")
  public RestResponse updateDefaultShippingAddress(@Valid @RequestBody DefaultAddressChangeRequest updateDefaultAddressRequest) {
    
    Long defaultShippingAddressId=updateDefaultAddressRequest.getAddressId();
    userService.updateDefaultShippingAddress(defaultShippingAddressId);
    
    return RestResponse.ok("Set default shipping address successfully");
  }

  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Set default shipping address for the authenticated user")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Set default shipping address successfully",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Address was not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
      })
  @PostMapping("/set-default-billing-address")
  public RestResponse updateDefaultBillingAddress(@Valid @RequestBody DefaultAddressChangeRequest updateDefaultAddressRequest) {
    
    Long defaultShippingAddressId=updateDefaultAddressRequest.getAddressId();
    userService.updateDefaultShippingAddress(defaultShippingAddressId);
    
    return RestResponse.ok("Set default billing address successfully");
  }
  
  @Operation(security = @SecurityRequirement(name = "bearerAuth"), summary = "Delete an address")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Delete address successfully",
              content =
                  @Content(
                      mediaType = "application/json",
                      schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "Address was not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @DeleteMapping("/{userAddressId}")
  public RestResponse deleteAddress(@PathVariable Long userAddressId) {
    
    userAddressService.deleteAddress(userAddressId);
    
    return RestResponse.ok("Delete address successfully");
  }
}
