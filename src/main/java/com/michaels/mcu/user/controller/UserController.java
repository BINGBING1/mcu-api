package com.michaels.mcu.user.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import com.michaels.mcu.shared.common.BusinessCode;
import com.michaels.mcu.shared.response.RestResponse;
import com.michaels.mcu.user.payload.request.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.michaels.mcu.security.annotation.RequireAuth;
import com.michaels.mcu.security.auth.AuthContext;
import com.michaels.mcu.shared.exception.BadRequestException;
import com.michaels.mcu.shared.exception.InvalidCredentialsException;
import com.michaels.mcu.shared.exception.NotFoundException;
import com.michaels.mcu.shared.exception.ServiceUnavailableException;
import com.michaels.mcu.shared.util.ValidateUtils;
import com.michaels.mcu.user.constants.UserTokenReceiverType;
import com.michaels.mcu.user.constants.UserTokenType;
import com.michaels.mcu.user.entity.User;
import com.michaels.mcu.user.entity.UserDeviceManagement;
import com.michaels.mcu.user.entity.UserToken;
import com.michaels.mcu.user.mail.service.MailService;
import com.michaels.mcu.user.payload.response.UserSignInResponse;
import com.michaels.mcu.user.service.UserService;
import com.michaels.mcu.user.service.UserTokenService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@CrossOrigin
@RestController
@RequestMapping("/user")
@Tag(
    name = "User Controller",
    description = "Contains endpoints for creating/retrieving/editing a user account")
public class UserController {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  private final UserService userService;
  private final UserTokenService userTokenService;
  private final MailService mailService;

  @Autowired
  public UserController(
      final UserService userService,
      final UserTokenService userTokenService,
      final MailService mailService) {

    this.userService = userService;
    this.userTokenService = userTokenService;
    this.mailService = mailService;
  }

  @Operation(summary = "Hard delete a user, FOR TEST ONLY!")
  @PostMapping("/delete-user")
  public RestResponse deleteUser(@Valid @RequestBody DeleteUserRequest request) {
    if(!"b6ca83f3-780e-4221-9785-2968057fe00a".equals(request.getToken())) {
      throw new InvalidCredentialsException(BusinessCode.MCU_ARGUMENT_NOT_VALID, "Invalid request!");
    }
    userService.deleteUserByEmail(request.getEmail());

    return RestResponse.ok("Delete user successfully");
  }

  @Operation(summary = "Register a new user account as a user")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Create user account successfully",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Passwords do not match",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "409",
            description = "User with given email already exists",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PostMapping("/registration")
  public RestResponse registration(@Valid @RequestBody RegistrationRequest userRegistrationRequest) {

    // password format check should removed to front end
    if(!ValidateUtils.validatePassword(userRegistrationRequest.getPassword())){
      throw new BadRequestException(BusinessCode.MCU_PASSWORD_FORMAT, "Password doesn't match format");
    }

    // create user
    final User user = userService.registerUser(userRegistrationRequest);

    // send the confirmation email
    final UserToken userToken =userTokenService.saveUserToken(
            user,
            UserTokenType.CONFIRM_EMAIL,
            UserTokenReceiverType.EMAIL,
            user.getEmail(),
            UUID.randomUUID().toString()
            );

    try {
      mailService.sendTokenEmail(
          user.getEmail(),
          user.getFirstName(),
          userToken.getToken(),
          UserTokenType.CONFIRM_EMAIL);
    }catch (Exception e){
        LOGGER.info("MCU_INTERNAL_ERROR", e);
        throw new ServiceUnavailableException(BusinessCode.MCU_INTERNAL_ERROR,
                "mail service doesn't work correctly");
    }
    // success message
    return RestResponse.ok("User account is created successfully");
  }


  @Operation(summary = "Send verification email to the user")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successfully send the email",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "User associate with this email not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PostMapping("/send-confirmation-email")
  public RestResponse sendConfirmationEmail(
      @Valid @RequestBody SendPasswordResetEmailRequest sendConfirmationEmailRequest) {

    // find user by email
    User user = userService
            .findUserByEmailOptional(sendConfirmationEmailRequest.getEmail())
            .orElseThrow(() -> {
                  throw new NotFoundException(BusinessCode.MCU_USR_NOT_FOUND, "Unable to find user");
                });

    // send the confirmation email
    final UserToken userToken =
        userTokenService.saveUserToken(
            user,
            UserTokenType.CONFIRM_EMAIL,
            UserTokenReceiverType.EMAIL,
            user.getEmail(),
            UUID.randomUUID().toString());

    mailService.sendConfirmationMail(user.getEmail(), user.getFirstName(), userToken.getToken());

    // success message
    return RestResponse.ok("Confirmation email sent");
  }


  @Operation(summary = "Confirm a user's email")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Account confirmed successfully",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = User.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Invalid token",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PostMapping("/user-confirmation")
  public RestResponse confirmUser(@Valid @RequestBody ConfirmUserRequest confirmUserRequest) {
    // confirm user with token
    User user = userService.confirmUser(confirmUserRequest.getToken());

    // success message
    return RestResponse.ok("Your email has been successfully verified");
  }


  @Operation(summary = "Send password reset email to the user")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successfully send the email",
            content =
                @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Your account hasn't been verified. Please verify your account at first",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "User associate with this email not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PostMapping("/send-reset-password-email")
  public RestResponse sendPasswordResetEmail(
          @Valid @RequestBody SendPasswordResetEmailRequest userVerificationRequest) {
      User user = userService.findUserByEmailOptional(userVerificationRequest.getEmail())
                      .orElseThrow(() -> {
                                  throw new NotFoundException(BusinessCode.MCU_USR_NOT_FOUND,
                                          "Unable to find user with give email");
                              });
      if (!user.getConfirmedAccount()) {
          throw new InvalidCredentialsException(BusinessCode.MCU_USR_NOT_VERIFY,
                  "Your account hasn't been verified. Please verify your account at first");
      }
      final UserToken userToken =
              userTokenService.saveUserToken(
                      user,
                      UserTokenType.RESET_PASSWORD,
                      UserTokenReceiverType.EMAIL,
                      user.getEmail(),
                      UUID.randomUUID().toString());
      mailService.sendPasswordResetMail(user.getEmail(), user.getFirstName(),userToken.getKey(), userToken.getToken());

      return RestResponse.ok("Send reset password email successfully");
  }


  @Operation(summary = "Verify whether the token for reset password is valid")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Reset password token is valid",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Invalid token",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),

      })
  @PostMapping("/verify-reset-password-email")
  public RestResponse verifyResetPassword(@Valid @RequestBody VerifyResetPasswordRequest request) {
    // verify whether the token is valid
    userService.verifyResetPasswordToken(request.getToken());

    // success message
    return RestResponse.ok("Reset password token is valid");
  }

  @Operation(summary = "Reset user's password via email")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Reset password successfully",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "400",
            description = "Password does not match the format",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "User not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PostMapping("/reset-password-via-email")
  public RestResponse resetPasswordViaEmail(@Valid @RequestBody ResetPasswordRequest request) {
    if(!ValidateUtils.validatePassword(request.getPassword())){
      throw new BadRequestException(BusinessCode.MCU_PASSWORD_FORMAT, "Password doesn't match the format");
    }
    // reset the password
    userService.resetPassword(request);

    // success message
    return RestResponse.ok("Password reset successfully");
  }


  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Update user's password")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Update the password successfully",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "400",
            description = "New password does not match format",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PostMapping("/update-password") @RequireAuth
  public RestResponse updatePassword(@Valid @RequestBody UpdatePasswordRequest request) {
    if(!ValidateUtils.validatePassword(request.getNewPassword())){
      throw new BadRequestException(BusinessCode.MCU_PASSWORD_FORMAT, "Password doesn't match format");
    }

    // update password
    userService.updatePassword(request.getPassword(), request.getNewPassword());

    // success message
    return RestResponse.ok("Password reset successfully");
  }


  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Update user's avatar, basic info and contact info")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "User's profile is updated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = User.class)))
      })
  @PostMapping("/profile") @RequireAuth
  public RestResponse<User> updateUserProfile(@Valid @RequestBody UpdateUserProfileRequest request) {
    if(!ValidateUtils.validateBirthday(request.getDateOfBirth())){
      throw new InvalidCredentialsException(BusinessCode.MCU_ARGUMENT_NOT_VALID ,"birthday is not valid");
    }

    User user = userService.updateUserProfile(request);
    return new RestResponse<>(user);
  }


    @Operation(
            security = @SecurityRequirement(name = "bearerAuth"),
            summary = "Update user's local Michaels store")
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "User's profile is updated",
                            content =
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = User.class)))
            })
    @PostMapping("/update-user-local-michaels-store") @RequireAuth
    public RestResponse<User> updateUserMichalesStore(@Valid @RequestBody UpdateUserLocalMichaelsStoreRequest request) {
        User user = userService.updateUserLocalMichaelsStore(request.getMichaelsStoreId());
        return new RestResponse<>(user);
    }

  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Swap Email")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Swap email successfully",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "401",
            description = "User is not authenticated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "User was not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PostMapping("/swap-email")
  public RestResponse swapEmail(@Valid @RequestBody SwapEmailRequest request) {
    // verify whether the new Email is the same
    if (!request.getOldEmail().equalsIgnoreCase(request.getNewEmail())) {
      throw new BadRequestException(BusinessCode.MCU_SAME_EMAIL,"New email address is same with the current email");
    }

    // swap email
    User user=userService.findUserByEmailOptional(request.getOldEmail())
        .orElseThrow(()->new NotFoundException(BusinessCode.MCU_USR_NOT_FOUND, "User not found"));

    userService.saveUserEmailSwapRequest(user.getId(), user.getEmail(), request.getNewEmail());

    // success message
    return RestResponse.ok("Email for swapping user email address had been sent");
  }


  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Get current authorized user")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Returned authorized user",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = User.class))),
        @ApiResponse(
            responseCode = "401",
            description = "User is not authenticated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "User was not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @GetMapping() @RequireAuth
  public RestResponse<User> getCurrentUser() {
    // get the user id from AuthContext
    AuthContext authContext = AuthContext.get();
    Long userId = authContext.getUserId();

    // find the user entity
    User user = userService.findUserByUserId(userId);

    return new RestResponse<>(user);
  }


  @Operation(
      summary = "Sign in")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Returned session token or 2-factor authentication token",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserSignInResponse.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Password incorrect, please check",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "User with given email was not found",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PostMapping("/sign-in")
  public RestResponse<UserSignInResponse> signIn(@Valid @RequestBody SignInRequest request) {
    UserSignInResponse response = userService.signIn(request);
    return new RestResponse<>(response);
  }


  @Operation(
      summary = "Sign out")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "401",
            description = "User is not authenticated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
      })
  @PostMapping("/sign-out") @RequireAuth
  public RestResponse signOut() {
    userService.signOut();
    return RestResponse.ok("Sign out successfully");
  }


  @Operation(
      summary = "Two-factor authentication")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Return session token",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserSignInResponse.class))),
        @ApiResponse(
            responseCode = "401",
            description = "Token already expired",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PostMapping("/two-factor-auth")
  public RestResponse<UserSignInResponse> twoFactorAuth(@Valid @RequestBody TwoFactorAuthRequest request) {
    UserSignInResponse response = userService.twoFactorAuth(request);
    return new RestResponse<>(response);
  }


  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Get user device list")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Return user device instances",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserDeviceManagement.class))),
        @ApiResponse(
            responseCode = "401",
            description = "User is not authenticated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @GetMapping("/user-device") @RequireAuth
  public RestResponse<List<UserDeviceManagement>> getUserDevices() {
    // update device settings
    List<UserDeviceManagement> userDeviceManagements = userService.getUserDeviceMangements();
    return new RestResponse<>(userDeviceManagements);
  }


  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Update device configuration")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Return modified UserDeviceManagement instance",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserDeviceManagement.class))),
        @ApiResponse(
            responseCode = "401",
            description = "User is not authenticated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @PatchMapping("/user-device/{deviceUuid}") @RequireAuth
  public RestResponse<UserDeviceManagement> updateUserDevice(
      @PathVariable String deviceUuid,
      @Valid @RequestBody UpdateUserDeviceRequest request
      ) {
    // update device settings
    UserDeviceManagement userDeviceManagement = userService.updateUserDeviceManagement(deviceUuid, request);
    return new RestResponse<>(userDeviceManagement);
  }


  @Operation(
      security = @SecurityRequirement(name = "bearerAuth"),
      summary = "Delete the device and related session")
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Delete device successfully",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = UserDeviceManagement.class))),
        @ApiResponse(
            responseCode = "401",
            description = "User is not authenticated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class))),
        @ApiResponse(
            responseCode = "404",
            description = "User is not authenticated",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = RestResponse.class)))
      })
  @DeleteMapping("/user-device/{deviceUuid}") @RequireAuth
  public RestResponse deleteDevice(@PathVariable String deviceUuid) {
    // delete device and related sessions
    userService.deleteUserDeviceManagement(deviceUuid);
    return RestResponse.ok("Delete device successfully");
  }

}
