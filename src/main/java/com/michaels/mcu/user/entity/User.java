package com.michaels.mcu.user.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.michaels.mcu.shared.common.BaseEntity;

import lombok.Builder;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "users")
public class User extends BaseEntity {

  protected String email;

  protected String recoveryEmail;

  protected String mobilePhone;

  protected Boolean mobilePhoneVerified;

  protected Integer accountStatus;

  protected String fromSource;

  protected String firstName;

  protected String lastName;

  protected LocalDate dateOfBirth;

  protected String passwordHash;

  protected String zipCode;

  protected String countryName;

  protected String language;

  protected String currency;

  protected String localMichaelsStoreId;

  protected Boolean enableTwoStepVerification;

  protected String userAvatarUrl;

  protected Long defaultBillingAddressId;

  protected Long defaultShippingAddressId;

  protected Boolean confirmedAccount;

  protected Boolean anonymous;

  protected String userTags;

  protected String interestedCategoryIds;

  protected Boolean joinedMichaelsRewards;

  protected Boolean emailSubscription;

  protected Boolean smsSubscription;

  protected Integer loginRetryTimes;

  protected LocalDateTime lastBadLoginTime;

  public User() {
    super();
  }
  
  @Builder
  public User(
      Long id, LocalDateTime createdTime, Long created_by, LocalDateTime updatedTime, Long updated_by,
      
      String email, String recoveryEmail, String mobilePhone, Boolean mobilePhoneVerified,
      Integer accountStatus, String fromSource, String firstName, String lastName, LocalDate dateOfBirth,
      String passwordHash, String zipCode, String countryName, String language, String currency,
      String localMichaelsStoreId, Boolean enableTwoStepVerification, String userAvatarUrl,
      Long defaultBillingAddressId, Long defaultShippingAddressId, Boolean confirmedAccount, Boolean anonymous,
      String userTags, String interestedCategoryIds, Boolean joinedMichaelsRewards, Boolean emailSubscription,
      Boolean smsSubscription, Integer loginRetryTimes, LocalDateTime lastBadLogin) {
    
    super(id, createdTime, created_by, updatedTime, updated_by);
    
    this.email = email;
    this.recoveryEmail = recoveryEmail;
    this.mobilePhone = mobilePhone;
    this.mobilePhoneVerified = mobilePhoneVerified;
    this.accountStatus = accountStatus;
    this.fromSource = fromSource;
    this.firstName = firstName;
    this.lastName = lastName;
    this.dateOfBirth = dateOfBirth;
    this.passwordHash = passwordHash;
    this.zipCode = zipCode;
    this.countryName = countryName;
    this.language = language;
    this.currency = currency;
    this.localMichaelsStoreId = localMichaelsStoreId;
    this.enableTwoStepVerification = enableTwoStepVerification;
    this.userAvatarUrl = userAvatarUrl;
    this.defaultBillingAddressId = defaultBillingAddressId;
    this.defaultShippingAddressId = defaultShippingAddressId;
    this.confirmedAccount = confirmedAccount;
    this.anonymous = anonymous;
    this.userTags = userTags;
    this.interestedCategoryIds = interestedCategoryIds;
    this.joinedMichaelsRewards = joinedMichaelsRewards;
    this.emailSubscription = emailSubscription;
    this.smsSubscription = smsSubscription;
    this.loginRetryTimes = loginRetryTimes;
    this.lastBadLoginTime = lastBadLogin;
  }
  // ========================================= getter and setter ============================================

  @Id
  @Column(name = "user_id")
  @JsonSerialize(using=ToStringSerializer.class)
  public Long getId() {
    return super.getId();
  }

  @Email
  @NotBlank(message = "Email cannot be blank")
  @Column(name = "email")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Email
  @Column(name = "recovery_email")
  public String getRecoveryEmail() {
    return recoveryEmail;
  }

  public void setRecoveryEmail(String recoveryEmail) {
    this.recoveryEmail = recoveryEmail;
  }

  @Column(name = "mobile_phone")
  public String getMobilePhone() {
    return mobilePhone;
  }

  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }

  @NotNull
  @Column(name = "is_mobile_phone_verified")
  public Boolean getMobilePhoneVerified() {
    return mobilePhoneVerified;
  }

  public void setMobilePhoneVerified(Boolean mobilePhoneVerified) {
    this.mobilePhoneVerified = mobilePhoneVerified;
  }

  @NotNull
  @Column(name = "account_status")
  public Integer getAccountStatus() {
    return accountStatus;
  }

  public void setAccountStatus(Integer accountStatus) {
    this.accountStatus = accountStatus;
  }

  @Column(name = "from_source")
  public String getFromSource() {
    return fromSource;
  }

  public void setFromSource(String fromSource) {
    this.fromSource = fromSource;
  }

  @NotBlank
  @Column(name = "first_name")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  @NotBlank
  @Column(name = "last_name")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @JsonFormat(pattern = "MM/dd/yyyy")
  @Column(name = "date_of_birth")
  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  @JsonIgnore
  @NotBlank(message = "User password cannot be blank")
  @Column(name = "password_hash")
  public String getPasswordHash() {
    return passwordHash;
  }

  public void setPasswordHash(String passwordHash) {
    this.passwordHash = passwordHash;
  }

  @Column(name = "zip_code")
  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  @Column(name = "country_name")
  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  @Column(name = "language")
  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  @Column(name = "currency")
  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  @Column(name = "local_michaels_store_id")
  public String getLocalMichaelsStoreId() {
    return localMichaelsStoreId;
  }

  public void setLocalMichaelsStoreId(String localMichaelsStoreId) {
    this.localMichaelsStoreId = localMichaelsStoreId;
  }

  @NotNull
  @Column(name = "two_step_verification", columnDefinition = "boolean default false")
  public Boolean getEnableTwoStepVerification() {
    return enableTwoStepVerification;
  }

  public void setEnableTwoStepVerification(Boolean enableTwoStepVerification) {
    this.enableTwoStepVerification = enableTwoStepVerification;
  }

  @Column(name = "user_avatar_url")
  public String getUserAvatarUrl() {
    return userAvatarUrl;
  }

  public void setUserAvatarUrl(String userAvatarUrl) {
    this.userAvatarUrl = userAvatarUrl;
  }

  @Column(name = "default_billing_address_id")
  public Long getDefaultBillingAddressId() {
    return defaultBillingAddressId;
  }

  public void setDefaultBillingAddressId(Long defaultBillingAddressId) {
    this.defaultBillingAddressId = defaultBillingAddressId;
  }

  @Column(name = "default_shipping_address_id")
  public Long getDefaultShippingAddressId() {
    return defaultShippingAddressId;
  }

  public void setDefaultShippingAddressId(Long defaultShippingAddressId) {
    this.defaultShippingAddressId = defaultShippingAddressId;
  }

  @NotNull
  @Column(name = "is_account_confirmed")
  public Boolean getConfirmedAccount() {
    return confirmedAccount;
  }

  public void setConfirmedAccount(Boolean confirmedAccount) {
    this.confirmedAccount = confirmedAccount;
  }

  @NotNull
  @Column(name = "is_anonymous")
  public Boolean getAnonymous() {
    return anonymous;
  }

  public void setAnonymous(Boolean anonymous) {
    this.anonymous = anonymous;
  }

  @Column(name = "user_tags")
  public String getUserTags() {
    return userTags;
  }

  public void setUserTags(String userTags) {
    this.userTags = userTags;
  }

  @Column(name = "interested_category_ids")
  public String getInterestedCategoryIds() {
    return interestedCategoryIds;
  }

  public void setInterestedCategoryIds(String interestedCategoryIds) {
    this.interestedCategoryIds = interestedCategoryIds;
  }

  @NotNull
  @Column(name = "is_michaels_rewards_member", columnDefinition = "boolean default false")
  public Boolean getJoinedMichaelsRewards() {
    return joinedMichaelsRewards;
  }

  public void setJoinedMichaelsRewards(Boolean joinedMichaelsRewards) {
    this.joinedMichaelsRewards = joinedMichaelsRewards;
  }

  @NotNull
  @Column(name = "email_subscription", columnDefinition = "boolean default false")
  public Boolean getEmailSubscription() {
    return emailSubscription;
  }

  public void setEmailSubscription(Boolean emailSubscription) {
    this.emailSubscription = emailSubscription;
  }

  @NotNull
  @Column(name = "sms_subscription", columnDefinition = "boolean default false")
  public Boolean getSmsSubscription() {
    return smsSubscription;
  }

  public void setSmsSubscription(Boolean smsSubscription) {
    this.smsSubscription = smsSubscription;
  }

  @NotNull
  @Column(name = "login_retry_times", columnDefinition = "tinyint default 0")
  public Integer getLoginRetryTimes() {
    return loginRetryTimes;
  }

  public void setLoginRetryTimes(Integer loginRetryTimes) {
    this.loginRetryTimes = loginRetryTimes;
  }

  @Column(name = "last_bad_login_time")
  public LocalDateTime getLastBadLogin() {
    return lastBadLoginTime;
  }

  public void setLastBadLogin(LocalDateTime lastBadLogin) {
    this.lastBadLoginTime = lastBadLogin;
  }


  @NotNull
  @CreatedDate
  @Column(name = "created_time")
  public LocalDateTime getCreatedTime() {
      return createdTime;
  }

  public void setCreatedTime(LocalDateTime createdTime) {
      this.createdTime = createdTime;
  }

  @NotNull
  @LastModifiedDate
  @Column(name = "updated_time")
  public LocalDateTime getUpdatedTime() {
      return updatedTime;
  }

  public void setUpdatedTime(LocalDateTime updatedTime) {
      this.updatedTime = updatedTime;
  }

}
