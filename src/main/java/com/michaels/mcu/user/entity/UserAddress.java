package com.michaels.mcu.user.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.michaels.mcu.shared.common.BaseEntity;

import lombok.Builder;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_address")
public class UserAddress extends BaseEntity {

  private Long userId;

  private Boolean active;

  private Boolean commercial;

  private String companyName;

  private String firstName;

  private String lastName;

  private String addressLine1;

  private String addressLine2;

  private String city;

  private String state;

  private Integer stateId;

  private String countryId;

  private String zipCode;

  private String telephone;

  private String fax;

  private String remark;

  private Boolean verified;
  
  
  public UserAddress() {
    
  }

  @Builder
  public UserAddress(
      Long id, LocalDateTime createdTime, Long created_by, LocalDateTime updatedTime, Long updated_by,
      
      Long userAddressId, Long userId, Boolean active, Boolean commercial, String companyName,
      String firstName, String lastName, String addressLine1, String addressLine2, String city, String state,
      Integer stateId, String countryId, String zipCode, String telephone, String fax, String remark,
      Boolean verified) {
    
    super(id, createdTime, created_by, updatedTime, updated_by);
    
    this.userId = userId;
    this.active = active;
    this.commercial = commercial;
    this.companyName = companyName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.addressLine1 = addressLine1;
    this.addressLine2 = addressLine2;
    this.city = city;
    this.state = state;
    this.stateId = stateId;
    this.countryId = countryId;
    this.zipCode = zipCode;
    this.telephone = telephone;
    this.fax = fax;
    this.remark = remark;
    this.verified = verified;
  }
  // ======================================== getter and setter===================================

  @Id
  @Column(name = "user_address_id")
  public Long getId() {
    return super.getId();
  }

  @NotNull
  @Column(name = "user_id")
  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @NotNull
  @Column(name = "is_active", columnDefinition = "boolean default true")
  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  @NotNull
  @Column(name = "is_commercial", columnDefinition = "boolean default false")
  public Boolean getCommercial() {
    return commercial;
  }

  public void setCommercial(Boolean commercial) {
    this.commercial = commercial;
  }

  @Column(name = "company_name")
  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  @NotBlank
  @Column(name = "first_name")
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  @NotBlank
  @Column(name = "last_name")
  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @NotBlank
  @Column(name = "address_line_1")
  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  @Column(name = "address_line_2")
  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  @NotBlank
  @Column(name = "city")
  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @NotBlank
  @Column(name = "state")
  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  @Column(name = "state_id")
  public Integer getStateId() {
    return stateId;
  }

  public void setStateId(Integer stateId) {
    this.stateId = stateId;
  }

  @Column(name = "country_id")
  public String getCountryId() {
    return countryId;
  }

  public void setCountryId(String countryId) {
    this.countryId = countryId;
  }

  @Column(name = "zipcode")
  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  @Column(name = "telephone")
  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  @Column(name = "fax")
  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  @Column(name = "remark")
  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  @NotNull
  @Column(name = "is_verified", columnDefinition = "boolean default true")
  public Boolean getVerified() {
    return verified;
  }

  public void setVerified(Boolean verified) {
    this.verified = verified;
  }


  @NotNull
  @CreatedDate
  @Column(name = "created_time")
  public LocalDateTime getCreatedTime() {
      return createdTime;
  }

  public void setCreatedTime(LocalDateTime createdTime) {
      this.createdTime = createdTime;
  }

  @NotNull
  @LastModifiedDate
  @Column(name = "updated_time")
  public LocalDateTime getUpdatedTime() {
      return updatedTime;
  }

  public void setUpdatedTime(LocalDateTime updatedTime) {
      this.updatedTime = updatedTime;
  }

}
