package com.michaels.mcu.user.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.michaels.mcu.shared.common.BaseEntity;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_device_management")
public class UserDeviceManagement extends BaseEntity {

  private Long userId;
  
  private String deviceUuid;
  
  private Integer deviceType;
  
  private String deviceName;
  
  private LocalDateTime lastLoginTime;
  
  private String lastLoginIp;
  
  private String lastLoginAddress;
  
  private Integer confirmedDevice;
  
  private Integer faceIdActive;
  
  private Integer touchIdActive;
  
  private Boolean isDeleted;
  
  @Override
  @Id
  @Column(name="user_device_management_id")
  public Long getId() {
    return super.getId();
  }
  
  @NotNull
  @Column(name="user_id")
  public Long getUserId() {
    return userId;
  }
  
  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @NotNull
  @Column(name="device_uuid")
  public String getDeviceUuid() {
    return deviceUuid;
  }

  public void setDeviceUuid(String deviceUuid) {
    this.deviceUuid = deviceUuid;
  }

  @NotNull
  @Column(name="device_type")
  public Integer getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(Integer deviceType) {
    this.deviceType = deviceType;
  }

  @NotNull
  @Column(name="device_name")
  public String getDeviceName() {
    return deviceName;
  }

  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }

  @NotNull
  @Column(name="last_login_time")
  public LocalDateTime getLastLoginTime() {
    return lastLoginTime;
  }

  public void setLastLoginTime(LocalDateTime lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  @NotNull
  @Column(name="last_login_ip")
  public String getLastLoginIp() {
    return lastLoginIp;
  }

  public void setLastLoginIp(String lastLoginIp) {
    this.lastLoginIp = lastLoginIp;
  }

  @NotNull
  @Column(name="last_login_address")
  public String getLastLoginAddress() {
    return lastLoginAddress;
  }

  public void setLastLoginAddress(String lastLoginAddress) {
    this.lastLoginAddress = lastLoginAddress;
  }

  @NotNull
  @Column(name="confirmed_device")
  public Integer getConfirmedDevice() {
    return confirmedDevice;
  }

  public void setConfirmedDevice(Integer confirmed_device) {
    this.confirmedDevice = confirmed_device;
  }

  @NotNull
  @Column(name="is_face_id_active")
  public Integer getFaceIdActive() {
    return faceIdActive;
  }

  public void setFaceIdActive(Integer faceIdActive) {
    this.faceIdActive = faceIdActive;
  }

  @NotNull
  @Column(name="is_touch_id_active")
  public Integer getTouchIdActive() {
    return touchIdActive;
  }

  public void setTouchIdActive(Integer touchIdActive) {
    this.touchIdActive = touchIdActive;
  }

  @NotNull
  @Column(name="is_deleted")
  public Boolean getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }
  
}
