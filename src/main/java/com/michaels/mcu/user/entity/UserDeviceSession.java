package com.michaels.mcu.user.entity;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

import com.michaels.mcu.shared.common.CouchBaseEntity;

import lombok.Builder;

@Document
public class UserDeviceSession extends CouchBaseEntity {
  
  private static final String USER_DEVICE_SESSION="UserDeviceSession";
  
  @Id
  private String key;
  
  @Field
  private String deviceUuid;
  
  @Field
  private Long userId;

  @Field
  private String sessionKey;

  @Field
  private LocalDateTime lastOperateTime;

  @Field
  private LocalDateTime createTime;

  @Field
  private LocalDateTime expirationTime;

  public UserDeviceSession() {
    super(USER_DEVICE_SESSION);
  }

  @Builder
  public UserDeviceSession(
      String key, 
      String deviceUuid, 
      Long userId, 
      String sessionKey,
      LocalDateTime lastOperationTime,
      LocalDateTime createTime,
      LocalDateTime expirationTime) {
    super();
    this.key = key;
    this.deviceUuid = deviceUuid;
    this.userId = userId;
    this.sessionKey = sessionKey;
    this.createTime = createTime;
    this.expirationTime = expirationTime;
  }

  public boolean hasExpired() {
    return LocalDateTime.now().isAfter(this.expirationTime);
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getDeviceUuid() {
    return deviceUuid;
  }

  public void setDeviceUuid(String deviceUuid) {
    this.deviceUuid = deviceUuid;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getSessionKey() {
    return sessionKey;
  }

  public void setSessionKey(String sessionKey) {
    this.sessionKey = sessionKey;
  }

  public LocalDateTime getLastOperateTime() {
    return lastOperateTime;
  }

  public void setLastOperateTime(LocalDateTime lastOperateTime) {
    this.lastOperateTime = lastOperateTime;
  }

  public LocalDateTime getCreateTime() {
    return createTime;
  }

  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

  public LocalDateTime getExpirationTime() {
    return expirationTime;
  }

  public void setExpirationTime(LocalDateTime expirationTime) {
    this.expirationTime = expirationTime;
  }
}
