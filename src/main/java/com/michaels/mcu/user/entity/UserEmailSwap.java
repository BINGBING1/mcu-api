package com.michaels.mcu.user.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.michaels.mcu.shared.common.BaseEntity;

import lombok.Builder;

@Entity
@Builder
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_email_swap_request")
public class UserEmailSwap extends BaseEntity {

    public static final long EXPIRATION = 7 * 24 * 3600 * 1000;

    private Long userId;

    private String token;

    private String oldEmail;

    private String newEmail;

    private LocalDateTime expirationTime;

    private Integer status;

    // ======================================= getter and setter =======================================

    public boolean hasExpired() {
        return LocalDateTime.now().isAfter(getExpirationTime());
    }

    @Override
    @Id
    @Column(name = "user_email_swap_request_id")
    public Long getId() {
      return super.getId();
    }

    @NotNull
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @NotBlank
    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @NotBlank
    @Email
    @Column(name = "old_email")
    public String getOldEmail() {
        return oldEmail;
    }

    public void setOldEmail(String oldEmail) {
        this.oldEmail = oldEmail;
    }

    @NotBlank
    @Email
    @Column(name = "new_email")
    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    @NotNull
    @Column(name = "expiration_time")
    public LocalDateTime getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(LocalDateTime expirationTime) {
        this.expirationTime = expirationTime;
    }

    @NotNull
    @Column(name = "status", columnDefinition = "tinyint")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
