package com.michaels.mcu.user.entity;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

import com.michaels.mcu.shared.common.CouchBaseEntity;

import lombok.Builder;

@Document
public class UserLog extends CouchBaseEntity {
  
  private static final String USER_LOG="UserLog";
  
  @Id
  private String key;
  
  @Field
  private String userId;
  
  @Field
  private String deviceUuid;
  
  @Field
  private Integer accessType;
  
  @Field
  private LocalDateTime accessTime;
  
  @Field
  private String description;
  
  @Field
  private String currentIp;
  
  @Field
  private String currentLocation;
  
  @Field
  private Boolean locationChangeNotify;

  @Builder
  public UserLog(String key, String userId, String deviceUuid, Integer accessType, LocalDateTime accessTime,
      String description, String currentIp, String currentLocation, Boolean locationChangeNotify) {
    super();
    this.key = key;
    this.userId = userId;
    this.deviceUuid = deviceUuid;
    this.accessType = accessType;
    this.accessTime = accessTime;
    this.description = description;
    this.currentIp = currentIp;
    this.currentLocation = currentLocation;
    this.locationChangeNotify = locationChangeNotify;
  }

  // ================================== getter and setter =================================
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Integer getAccessType() {
    return accessType;
  }

  public void setAccessType(Integer accessType) {
    this.accessType = accessType;
  }

  public LocalDateTime getAccessTime() {
    return accessTime;
  }

  public void setAccessTime(LocalDateTime accessTime) {
    this.accessTime = accessTime;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getCurrentIp() {
    return currentIp;
  }

  public void setCurrentIp(String currentIp) {
    this.currentIp = currentIp;
  }

  public String getCurrentLocation() {
    return currentLocation;
  }

  public void setCurrentLocation(String currentLocation) {
    this.currentLocation = currentLocation;
  }

  public Boolean getLocationChangeNotify() {
    return locationChangeNotify;
  }

  public void setLocationChangeNotify(Boolean locationChangeNotify) {
    this.locationChangeNotify = locationChangeNotify;
  }

  public String getDeviceUuid() {
    return deviceUuid;
  }

  public void setDeviceUuid(String deviceUuid) {
    this.deviceUuid = deviceUuid;
  }

  public static String getUserLog() {
    return USER_LOG;
  }
  
  
}
