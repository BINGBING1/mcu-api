package com.michaels.mcu.user.entity;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

import com.michaels.mcu.shared.common.CouchBaseEntity;
import com.michaels.mcu.user.constants.UserTokenReceiverType;
import com.michaels.mcu.user.constants.UserTokenType;

import lombok.Builder;

@Document
public class UserToken extends CouchBaseEntity {
  
  private static final String USER_TOKEN="UserToken";
  
  @Id
  private String key;

  @Field
  private Long userId;

  @Field
  private Integer tokenType;

  @Field
  private Integer receiverType;

  @Field
  private String receiverAddress;

  @Field
  private String token;

  @Field
  private LocalDateTime createTime;

  @Field
  private LocalDateTime expirationTime;

  public UserToken() {
    super(USER_TOKEN);
  }

  @Builder
  public UserToken(
      final String key, 
      final Long userId, 
      final UserTokenType tokenType,
      final UserTokenReceiverType receiverType, 
      final String receiverAddress, 
      final String token) {
    this(
        key, 
        userId, 
        tokenType.getValue(),
        receiverType.getValue(), 
        receiverAddress,
        token, 
        tokenType.getExpirationTime()
        );
  }

  public UserToken(
      final String key, 
      final Long userId, 
      final Integer tokenType, 
      final Integer receiverType, 
      final String receiverAddress,
      final String token, 
      final Long expirationTimeMilli
      ) {
    super(USER_TOKEN);
    
    this.key = key;
    this.userId = userId;
    this.tokenType = tokenType;
    this.receiverType = receiverType;
    this.receiverAddress = receiverAddress;
    this.token = token;
    this.createTime = LocalDateTime.now();
    this.expirationTime = this.createTime.plus(expirationTimeMilli, ChronoUnit.MILLIS);
  }

  public boolean hasExpired() {
    return LocalDateTime.now().isAfter(this.expirationTime);
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Integer getTokenType() {
    return tokenType;
  }

  public void setTokenType(Integer tokenType) {
    this.tokenType = tokenType;
  }

  public Integer getReceiverType() {
    return receiverType;
  }

  public void setReceiverType(Integer receiverType) {
    this.receiverType = receiverType;
  }

  public String getReceiverAddress() {
    return receiverAddress;
  }

  public void setReceiverAddress(String receiverAddress) {
    this.receiverAddress = receiverAddress;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public LocalDateTime getCreateTime() {
    return createTime;
  }

  public void setCreateTime(LocalDateTime createTime) {
    this.createTime = createTime;
  }

  public LocalDateTime getExpirationTime() {
    return expirationTime;
  }

  public void setExpirationTime(LocalDateTime expirationTime) {
    this.expirationTime = expirationTime;
  }

}
