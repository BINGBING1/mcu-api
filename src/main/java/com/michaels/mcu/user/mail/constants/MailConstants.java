package com.michaels.mcu.user.mail.constants;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class MailConstants {

  private final String host;
  private final String apiKey;
  private final String fromAddress;
  private final String fromName;
  private final String confirmationTemplateId;
  private final String confirmationPath;
  private final String passwordResetTemplateId;
  private final String passwordResetPath;
  private final String passwordChangeTemplateId;
  private final String passwordChangePath;
  private final String subscriptionTemplateId;
  private final String subscriptionPath;
  private final String linkRewardTemplateId;
  private final String linkRewardPath;

  @Autowired
  public MailConstants(
      @Value("${sendgrid.host}") String host,
      @Value("${sendgrid.apiKey}") String apiKey,
      @Value("${sendgrid.from.address}") String fromAddress,
      @Value("${sendgrid.from.name}") String fromName,
      @Value("${sendgrid.confirmation.templateId}") String confirmationTemplateId,
      @Value("${sendgrid.confirmation.path}") String confirmationPath,
      @Value("${sendgrid.password.reset.templateId}") String passwordResetTemplateId,
      @Value("${sendgrid.password.change.templateId}") String passwordChangeTemplateId,
      @Value("${sendgrid.subscription.templateId}") String subscriptionTemplateId,
      @Value("${sendgrid.subscription.path}") String subscriptionPath,
      @Value("${sendgrid.reward.link.templateId}") String linkRewardTemplateId,
      @Value("${sendgrid.reward.link.path}") String linkRewardPath) {
    this.host = host;
    this.apiKey = apiKey;
    this.fromAddress = fromAddress;
    this.fromName = fromName;

    this.confirmationTemplateId = confirmationTemplateId;
    this.confirmationPath = confirmationPath;

    this.passwordResetTemplateId = passwordResetTemplateId;
    this.passwordResetPath = "";

    this.passwordChangeTemplateId = passwordChangeTemplateId;
    this.passwordChangePath = "";

    this.subscriptionTemplateId = subscriptionTemplateId;
    this.subscriptionPath = subscriptionPath;
    
    //@TODO set real variable in yml when Sendgrid template is ready
    this.linkRewardTemplateId = linkRewardTemplateId;
    this.linkRewardPath = linkRewardPath;
  }
}
