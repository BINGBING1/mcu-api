package com.michaels.mcu.user.mail.service;

import com.michaels.mcu.shared.common.BusinessCode;
import com.michaels.mcu.shared.exception.ServiceUnavailableException;
import com.michaels.mcu.user.constants.UserTokenType;
import com.michaels.mcu.user.mail.constants.MailConstants;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.Personalization;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class MailService {
  private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

  private final MailConstants mailConstants;

  @Autowired
  public MailService(final MailConstants mailConstants) {
    this.mailConstants = mailConstants;
  }

  public boolean sendTokenEmail(
      final String email, 
      final String firstName, 
      final String token, 
      final UserTokenType userTokenType) {
    LOGGER.debug("MailService sendTokenEmail start. Send token type {}", userTokenType);
    
    String path = null;
    String templateId = null;
    if (userTokenType == UserTokenType.CONFIRM_EMAIL) {
      path = mailConstants.getConfirmationPath();
      templateId = mailConstants.getConfirmationTemplateId();
    } else if (userTokenType == UserTokenType.RESET_PASSWORD) {
      path = mailConstants.getPasswordResetPath();
      templateId = mailConstants.getPasswordResetTemplateId();
    } else if (userTokenType == UserTokenType.LINK_REWARD) {
      path = mailConstants.getLinkRewardPath();
      templateId = mailConstants.getLinkRewardTemplateId();
    }

    if (path == null || templateId == null) {
      LOGGER.debug("Unsupported token type {}", userTokenType);
      return false;
    }
    Mail mail = buildTokenMail(
        email, 
        firstName, 
        token, 
        path, 
        templateId);
    
    LOGGER.debug("MailService sendTokenEmail end");
    return sendMail(mail);
  }

  public boolean sendConfirmationMail(
      final String email, 
      final String firstName, 
      final String token) {
    LOGGER.debug("MailService sendConfirmationMail start");
    Mail mail =
        buildTokenMail(
            email,
            firstName,
            token,
            mailConstants.getConfirmationPath(),
            mailConstants.getConfirmationTemplateId());
    LOGGER.debug("MailService sendConfirmationMail end");
    return sendMail(mail);
  }

  public boolean sendPasswordResetMail(
      final String email, 
      final String firstName, 
      final String tokenKey, 
      final String token) {
    LOGGER.debug("MailService sendPasswordResetMail start");
    
    Mail mail =
        buildTokenMail(
            email,
            firstName,
            token,
            mailConstants.getPasswordResetPath(),
            mailConstants.getPasswordResetTemplateId());
    boolean sent = sendMail(mail);
    
    LOGGER.debug("MailService sendPasswordResetMail end");
    return sent;
  }
  
  public boolean sendTwoFactorAuthMail(
      final String email,
      final String firstName,
      final String token
      ) {
    Mail mail =
        buildTokenMail(
            email,
            firstName,
            token,
            mailConstants.getPasswordResetPath(),
            mailConstants.getPasswordResetTemplateId());
    boolean sent = sendMail(mail);
    return sent;
  }

  public Mail buildTokenMail(
      final String email,
      final String firstName,
      final String token,
      final String path,
      final String template) {

    LOGGER.debug("MailService buildTokenMail start");

    Mail mail = new Mail();
    Email fromEmail = new Email();
    fromEmail.setName(mailConstants.getFromName());
    fromEmail.setEmail(mailConstants.getFromAddress());
    mail.setFrom(fromEmail);
    mail.setTemplateId(template);

    Personalization personalization = new Personalization();
    personalization.addDynamicTemplateData("firstName", firstName);
    personalization.addDynamicTemplateData("host", mailConstants.getHost());
    if (!path.isBlank()) {
      personalization.addDynamicTemplateData("path", path);
    }
    personalization.addDynamicTemplateData("token", token);
    personalization.addTo(new Email(email));
    mail.addPersonalization(personalization);
    
    LOGGER.debug("MailService buildTokenMail end");
    return mail;
  }

  public boolean sendMail(final Mail mail) {
    LOGGER.debug("MailService SendMail start");

    SendGrid sendGrid = new SendGrid(mailConstants.getApiKey());
    sendGrid.addRequestHeader("X-Mock", "true");

    Request request = new Request();

    try {
      request.setMethod(Method.POST);
      request.setEndpoint("mail/send");
      request.setBody(mail.build());
      Response response = sendGrid.api(request);

      if (response.getStatusCode() == HttpStatus.SC_ACCEPTED) {
        LOGGER.debug("MailService SendMail end");
        return true;
      }
    } catch (IOException ex) {
      LOGGER.debug(ex.getMessage());
      throw new ServiceUnavailableException(BusinessCode.MCU_INTERNAL_ERROR, "Failed to send via SendGrid");
    }
    return false;
  }
}
