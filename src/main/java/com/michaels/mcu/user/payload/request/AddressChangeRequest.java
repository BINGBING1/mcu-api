package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AddressChangeRequest {

  @NotNull
  private Boolean commercial;

  private String companyName;

  private String firstName;

  private String lastName;

  @NotBlank
  private String addressLine1;

  private String addressLine2;

  @NotBlank
  private String cityId;

  @NotBlank
  private String stateId;
  
  @NotBlank
  private String countryId;

  @NotBlank
  private String zipCode;
  
  private String telephone;

  private String fax;
  
  private String remark;


}
