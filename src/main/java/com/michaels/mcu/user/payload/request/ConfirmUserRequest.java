package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.NotBlank;

public class ConfirmUserRequest {

  @NotBlank
  public String token;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
  
  
}
