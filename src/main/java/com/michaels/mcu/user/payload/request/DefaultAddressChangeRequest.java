package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.NotNull;

public class DefaultAddressChangeRequest {
  
  @NotNull
  private Long addressId;

  public Long getAddressId() {
    return addressId;
  }

  public void setAddressId(Long addressId) {
    this.addressId = addressId;
  }
  
  
}
