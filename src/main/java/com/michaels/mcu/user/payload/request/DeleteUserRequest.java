package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.NotBlank;

public class DeleteUserRequest {
  @NotBlank
  private String email;
  
  @NotBlank
  private String token;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String tokenn) {
    this.token = tokenn;
  }
  
  
}
