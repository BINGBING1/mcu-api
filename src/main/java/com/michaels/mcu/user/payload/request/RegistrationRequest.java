package com.michaels.mcu.user.payload.request;

import com.michaels.mcu.shared.validation.ValidatePhone;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RegistrationRequest {
  @NotBlank 
  private String firstName;

  @NotBlank 
  private String lastName;

  @Email(message = "Invalid email")
  @NotBlank(message = "Email cannot be blank")
  private String email;

  @NotBlank 
  private String password;

  @ValidatePhone
  private String phoneNumber;

  @NotNull 
  private Boolean signUpForEmails;

  @NotNull 
  private Boolean signUpForTextMessage;

  @NotNull 
  private Boolean signUpForRewards;
  
}
