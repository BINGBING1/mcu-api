package com.michaels.mcu.user.payload.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ResetPasswordRequest {


  @NotBlank 
  private String password;
  
  @NotBlank 
  private String token;

  
  
}
