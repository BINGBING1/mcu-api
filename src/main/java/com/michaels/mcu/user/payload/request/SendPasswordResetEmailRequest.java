package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class SendPasswordResetEmailRequest {
  
  @NotBlank(message = "email cannot be blank")
  @Email
  private String email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
  
  
}
