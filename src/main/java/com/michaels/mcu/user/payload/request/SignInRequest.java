package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class SignInRequest {
  
  @Email @NotBlank 
  private String email;

  @NotBlank 
  private String password;
  
  @NotBlank
  private String deviceUuid;
  
  @NotNull
  private Integer deviceType;

  @NotBlank
  private String deviceName;
  
  @NotBlank
  private String loginIp;
  
  @NotBlank
  private String loginAddress;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getDeviceUuid() {
    return deviceUuid;
  }

  public void setDeviceUuid(String deviceUuid) {
    this.deviceUuid = deviceUuid;
  }

  public Integer getDeviceType() {
    return deviceType;
  }

  public String getDeviceName() {
    return deviceName;
  }

  public void setDeviceName(String deviceName) {
    this.deviceName = deviceName;
  }

  public void setDeviceType(Integer deviceType) {
    this.deviceType = deviceType;
  }

  public String getLoginIp() {
    return loginIp;
  }

  public void setLoginIp(String loginIp) {
    this.loginIp = loginIp;
  }

  public String getLoginAddress() {
    return loginAddress;
  }

  public void setLoginAddress(String loginAddress) {
    this.loginAddress = loginAddress;
  }
  
}
