package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.NotBlank;

public class SwapEmailRequest {

  @NotBlank
  private String token;
  
  @NotBlank
  private String oldEmail;
  
  @NotBlank
  private String newEmail;
  
  public String getToken() {
    return token;
  }
  
  public void setToken(String token) {
    this.token = token;
  }
  
  public String getOldEmail() {
    return oldEmail;
  }
  
  public void setOldEmail(String oldEmail) {
    this.oldEmail = oldEmail;
  }
  
  public String getNewEmail() {
    return newEmail;
  }
  
  public void setNewEmail(String newEmail) {
    this.newEmail = newEmail;
  }

}
