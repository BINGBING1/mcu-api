package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.NotBlank;

public class TwoFactorAuthRequest {

  @NotBlank
  private String token;
  
  @NotBlank
  private String verifyCode;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getVerifyCode() {
    return verifyCode;
  }

  public void setVerifyCode(String verifyCode) {
    this.verifyCode = verifyCode;
  }
  
  
}
