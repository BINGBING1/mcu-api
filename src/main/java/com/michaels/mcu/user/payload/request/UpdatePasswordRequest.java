package com.michaels.mcu.user.payload.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UpdatePasswordRequest {
  
  @NotBlank
  public String password;
  
  @NotBlank
  public String newPassword;
}
