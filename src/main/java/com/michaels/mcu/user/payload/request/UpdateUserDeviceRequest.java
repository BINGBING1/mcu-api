package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.NotNull;

public class UpdateUserDeviceRequest {
  @NotNull
  private Integer faceIdActive;
  
  @NotNull
  private Integer touchIdActive;

  public Integer getFaceIdActive() {
    return faceIdActive;
  }

  public void setFaceIdActive(Integer faceIdActive) {
    this.faceIdActive = faceIdActive;
  }

  public Integer getTouchIdActive() {
    return touchIdActive;
  }

  public void setTouchIdActive(Integer touchIdActive) {
    this.touchIdActive = touchIdActive;
  }
  
}
