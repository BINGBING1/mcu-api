package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.NotBlank;

public class UpdateUserLocalMichaelsStoreRequest {
    @NotBlank
    private String michaelsStoreId;

    public String getMichaelsStoreId() {
        return michaelsStoreId;
    }

    public void setMichaelsStoreId(String michaelsStoreId) {
        this.michaelsStoreId = michaelsStoreId;
    }
}
