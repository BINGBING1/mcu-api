package com.michaels.mcu.user.payload.request;

import com.michaels.mcu.shared.validation.ValidatePhone;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UpdateUserProfileRequest {

  private String userAvatarUrl;

  @NotBlank 
  private String firstName;

  @NotBlank 
  private String lastName;

  private String dateOfBirth;

  @ValidatePhone
  private String mobilePhone;

}
