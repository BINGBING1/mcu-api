package com.michaels.mcu.user.payload.request;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class VerifyResetPasswordRequest {

  @NotNull
  private String token;
  
}
