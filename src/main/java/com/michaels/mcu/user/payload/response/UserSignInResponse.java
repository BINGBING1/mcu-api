package com.michaels.mcu.user.payload.response;

import com.michaels.mcu.user.entity.User;

public class UserSignInResponse {
  
    private User user;
    private String token;
    
    private Boolean requireTwoFactorAuth;
    private String twoFactorAuthToken;
    
    public User getUser() {
      return user;
    }
    
    public void setUser(User user) {
      this.user = user;
    }
    
    public String getToken() {
      return token;
    }
    
    public void setToken(String token) {
      this.token = token;
    }

    public Boolean getRequireTwoFactorAuth() {
      return requireTwoFactorAuth;
    }

    public void setRequireTwoFactorAuth(Boolean requireTwoFactorAuth) {
      this.requireTwoFactorAuth = requireTwoFactorAuth;
    }

    public String getTwoFactorAuthToken() {
      return twoFactorAuthToken;
    }

    public void setTwoFactorAuthToken(String twoFactorAuthToken) {
      this.twoFactorAuthToken = twoFactorAuthToken;
    }
    
}
