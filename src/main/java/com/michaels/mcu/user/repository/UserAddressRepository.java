package com.michaels.mcu.user.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.michaels.mcu.user.entity.UserAddress;

@Repository
public interface UserAddressRepository extends JpaRepository<UserAddress, Long> {
  
  public Optional<UserAddress> findByIdAndUserId(Long id, Long userId);
  
  public List<UserAddress> findAllByUserId(Long userId);
  
}
