package com.michaels.mcu.user.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.michaels.mcu.user.entity.UserDeviceManagement;

@Repository
public interface UserDeviceManagementRepository extends JpaRepository<UserDeviceManagement, Long> {

  Optional<UserDeviceManagement> findByDeviceUuid(String deviceUuid);
  
  Optional<UserDeviceManagement> findByUserIdAndDeviceUuid(Long userId, String deviceUuid);
  
  List<UserDeviceManagement> findAllByUserId(Long userId);
  
}
