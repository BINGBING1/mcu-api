package com.michaels.mcu.user.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.michaels.mcu.user.entity.UserDeviceSession;

@Repository
public interface UserDeviceSessionRepository extends CouchbaseRepository<UserDeviceSession, String> {

  Optional<UserDeviceSession> findByDeviceUuid(String deviceUuid);
  
  List<UserDeviceSession> findAllByDeviceUuid(String deviceUuid);
  
}
