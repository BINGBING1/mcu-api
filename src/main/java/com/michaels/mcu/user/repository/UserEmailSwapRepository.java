package com.michaels.mcu.user.repository;

import com.michaels.mcu.user.entity.UserEmailSwap;
import com.michaels.mcu.user.payload.request.SwapEmailRequest;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserEmailSwapRepository extends JpaRepository<UserEmailSwap, Long> {

    Optional<SwapEmailRequest> findByOldEmail(String email);
  
    Optional<UserEmailSwap> findByToken(String token);
}
