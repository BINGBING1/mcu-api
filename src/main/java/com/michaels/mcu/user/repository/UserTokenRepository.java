package com.michaels.mcu.user.repository;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import com.michaels.mcu.user.entity.UserToken;

import java.util.Optional;

@Repository
public interface UserTokenRepository extends CouchbaseRepository<UserToken, String> {
  Optional<UserToken> findByToken(String token);
}
