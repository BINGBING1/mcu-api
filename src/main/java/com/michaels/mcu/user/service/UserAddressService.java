package com.michaels.mcu.user.service;

import java.util.List;

import com.michaels.mcu.shared.common.BusinessCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.michaels.mcu.security.auth.AuthContext;
import com.michaels.mcu.shared.exception.BadRequestException;
import com.michaels.mcu.shared.exception.NotFoundException;
import com.michaels.mcu.shared.generator.util.IdGeneratorUtils;
import com.michaels.mcu.user.entity.User;
import com.michaels.mcu.user.entity.UserAddress;
import com.michaels.mcu.user.payload.request.AddressChangeRequest;
import com.michaels.mcu.user.repository.UserAddressRepository;

@Service
public class UserAddressService {
  private static final Logger LOGGER = LoggerFactory.getLogger(UserAddressService.class);

  @Autowired
  private UserService userService;
  
  @Autowired
  private UserAddressRepository userAddressRepository;
  
  @Transactional
  public List<UserAddress> getAddressList() {
    // get the auth context
    final AuthContext authContext = AuthContext.get();
    final Long userId = authContext.getUserId();
    
    // get user address
    final List<UserAddress> userAddresses = userAddressRepository.findAllByUserId(userId);
    
    return userAddresses;
  }
  
  @Transactional
  public UserAddress createAddress(AddressChangeRequest request) {
    // get the auth context
    final AuthContext authContext = AuthContext.get();
    final Long userId = authContext.getUserId();
    
    // create user address instance
    final UserAddress preparedAddress =
        UserAddress.builder()
            .id(IdGeneratorUtils.getUID())
            .userId(userId)
            .commercial(request.getCommercial())
            .companyName(request.getCompanyName())
            .firstName(request.getFirstName())
            .lastName(request.getLastName())
            .addressLine1(request.getAddressLine1())
            .addressLine2(request.getAddressLine2())
            .city(request.getCityId())
            .state(request.getStateId())
            .countryId(request.getCountryId())
            .zipCode(request.getZipCode())
            .telephone(request.getTelephone())
            .fax(request.getFax())
            .active(true)
            .verified(false)
            .build();
    UserAddress userAddress = userAddressRepository.save(preparedAddress);

    // set as default address not set
    final User user = userService.findUserByUserId(userId);
    if (user.getDefaultShippingAddressId() == null) {
      userService.updateDefaultShippingAddress(userAddress.getId());
      LOGGER.debug("this user has not default shipping address, set it as {}", userAddress.getId());
    }
    if (user.getDefaultBillingAddressId() == null) {
      userService.updateDefaultBillingAddress(userAddress.getId());
      LOGGER.debug("this user has not default billing address, set it as {}", userAddress.getId());
    }
    
    // return the created address instance
    return userAddress;
  }

  @Transactional
  public UserAddress updateAddress(AddressChangeRequest request, Long userAddressId) {
    // get the auth context
    final AuthContext authContext = AuthContext.get();
    final Long userId = authContext.getUserId();
    
    // get user address
    final UserAddress userAddress = userAddressRepository
            .findByIdAndUserId(userAddressId, userId)
            .orElseThrow(() -> new NotFoundException(BusinessCode.MCU_ADDRESS_NOT_FOUND, "Address not found"));
    
    userAddress.setCommercial(request.getCommercial());
    userAddress.setCompanyName(request.getCompanyName());
    userAddress.setFirstName(request.getFirstName());
    userAddress.setLastName(request.getLastName());
    userAddress.setAddressLine1(request.getAddressLine1());
    userAddress.setAddressLine2(request.getAddressLine2());
    userAddress.setCity(request.getCityId());
    userAddress.setState(request.getStateId());
    userAddress.setCountryId(request.getCountryId());
    userAddress.setZipCode(request.getZipCode());
    userAddress.setTelephone(request.getTelephone());
    userAddress.setFax(request.getFax());
    
    return userAddressRepository.save(userAddress);
  }
  
  @Transactional
  public void verifyAddress(Long userAddressId) {
    // get the auth context
    final AuthContext authContext = AuthContext.get();
    final Long userId = authContext.getUserId();
    
    // get user address
    final UserAddress userAddress = userAddressRepository
            .findByIdAndUserId(userAddressId, userId)
            .orElseThrow(() -> new NotFoundException(BusinessCode.MCU_ADDRESS_NOT_FOUND, "Address not found"));
    
    // TODO: might need to invoke 3rd API to verify the address
    userAddress.setVerified(true);
    userAddressRepository.save(userAddress);
  }

  @Transactional
  public void deleteAddress(Long userAddressId) {
    final UserAddress userAddress =
        userAddressRepository
            .findById(userAddressId)
            .orElseThrow(() -> new NotFoundException(BusinessCode.MCU_ADDRESS_NOT_FOUND, "Address is not found"));
    
    final Long userId = AuthContext.get().getUserId();
    if(!userAddress.getUserId().equals(userId)) {
      throw new BadRequestException(BusinessCode.MCU_ADDRESS_NOT_BELONG, "Address does not belong to current user");
    }
    
    final User user = userService.findUserByUserId(userId);
    if (userAddressId.equals(user.getDefaultShippingAddressId())) {
      userService.clearDefaultShippingAddress();
    }
    if (userAddressId.equals(user.getDefaultBillingAddressId())) {
      userService.clearDefaultBillingAddress();
    }
    userAddressRepository.deleteById(userAddressId);
  }

}
