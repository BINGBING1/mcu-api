package com.michaels.mcu.user.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import com.michaels.mcu.shared.common.BusinessCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.michaels.mcu.security.auth.AuthContext;
import com.michaels.mcu.shared.exception.BadRequestException;
import com.michaels.mcu.shared.exception.DuplicateException;
import com.michaels.mcu.shared.exception.InvalidCredentialsException;
import com.michaels.mcu.shared.exception.NotFoundException;
import com.michaels.mcu.shared.generator.util.IdGeneratorUtils;
import com.michaels.mcu.user.constants.UserAccountStatus;
import com.michaels.mcu.user.constants.UserTokenReceiverType;
import com.michaels.mcu.user.constants.UserTokenType;
import com.michaels.mcu.user.entity.User;
import com.michaels.mcu.user.entity.UserAddress;
import com.michaels.mcu.user.entity.UserDeviceManagement;
import com.michaels.mcu.user.entity.UserDeviceSession;
import com.michaels.mcu.user.entity.UserEmailSwap;
import com.michaels.mcu.user.entity.UserToken;
import com.michaels.mcu.user.mail.service.MailService;
import com.michaels.mcu.user.payload.request.RegistrationRequest;
import com.michaels.mcu.user.payload.request.ResetPasswordRequest;
import com.michaels.mcu.user.payload.request.SignInRequest;
import com.michaels.mcu.user.payload.request.TwoFactorAuthRequest;
import com.michaels.mcu.user.payload.request.UpdateUserDeviceRequest;
import com.michaels.mcu.user.payload.request.UpdateUserProfileRequest;
import com.michaels.mcu.user.payload.response.UserSignInResponse;
import com.michaels.mcu.user.repository.UserAddressRepository;
import com.michaels.mcu.user.repository.UserDeviceManagementRepository;
import com.michaels.mcu.user.repository.UserDeviceSessionRepository;
import com.michaels.mcu.user.repository.UserEmailSwapRepository;
import com.michaels.mcu.user.repository.UserRepository;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class UserService {
  private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
  
  private static final String TWO_FACTOR_JWT_TOKEN_SECRET = "13428049692442670";
  private static final long USER_SESSION_EXPIRATION_TIME = 30L * 24L * 3600L * 1000L;
  
  private static final String DEVICE_UUID="deviceUuid";

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private UserEmailSwapRepository userEmailSwapRepository;
  @Autowired
  private UserAddressRepository userAddressRepository;
  @Autowired
  private UserDeviceManagementRepository userDeviceManagementRepository;
  @Autowired
  private UserDeviceSessionRepository userDeviceSessionRepository;
  @Autowired @Lazy
  private UserTokenService userTokenService;
  @Autowired
  private MailService mailService;

  @Autowired
  private PasswordEncoder passwordEncoder;
  
  public User findUserByUserId(Long userId) {
    return userRepository
        .findById(userId)
        .orElseThrow(() -> {
              throw new NotFoundException(BusinessCode.MCU_USR_NOT_FOUND, "User not found");
            });
  }
  
  public User findUserByEmail(String email) {
    return userRepository
        .findByEmail(email)
        .orElseThrow(() -> {
              throw new NotFoundException(BusinessCode.MCU_USR_NOT_FOUND, "User not found");
            });
  }
  
  public Optional<User> findUserByEmailOptional(String email) {
    return userRepository.findByEmail(email);
  }
  
  public UserDeviceManagement findUserDeviceManagementByDeviceUuid(String deviceUuid) {
    return userDeviceManagementRepository.findByDeviceUuid(deviceUuid)
        .orElseThrow(()-> new NotFoundException(BusinessCode.MCU_DEVICE_NOT_FOUND, "Device not found!"));
  }
  
  public Optional<UserDeviceManagement> findUserDeviceManagementByDeviceUuidOptional(String deviceUuid) {
    return userDeviceManagementRepository.findByDeviceUuid(deviceUuid);
  }
  
  public UserDeviceSession findUserDeviceSessionByToken(String token) {
    return userDeviceSessionRepository.findById(token)
        .orElseThrow(()-> new NotFoundException(BusinessCode.MCU_DEVICE_SESSION_NOT_FOUND, "Device session not found!"));
  }

  public Optional<UserDeviceSession> findUserDeviceSessionByTokenOptional(String token) {
    return userDeviceSessionRepository.findById(token);
  }

  @Transactional
  public UserDeviceSession refreshUserDeviceSession(UserDeviceSession userDeviceSession) {
    userDeviceSession.setLastOperateTime(LocalDateTime.now());
    userDeviceSession.setExpirationTime(userDeviceSession.getLastOperateTime().plus(USER_SESSION_EXPIRATION_TIME, ChronoUnit.MILLIS));
    return userDeviceSessionRepository.save(userDeviceSession);
  }
  
  @Transactional
  public UserSignInResponse signIn(SignInRequest userSignInRequest) {
    User user = findUserByEmail(userSignInRequest.getEmail());
    if (!passwordEncoder.matches(userSignInRequest.getPassword(), user.getPasswordHash())) {
      // update login retry times
      user.setLoginRetryTimes(user.getLoginRetryTimes()+1);
      user.setLastBadLogin(LocalDateTime.now());
      throw new InvalidCredentialsException(BusinessCode.MCU_EMAIL_PASSWORD_WRONG, "Email or password does not match our records");
    }else {
      user.setLoginRetryTimes(0);
    }
    
    // create the device if new
    UserDeviceManagement userDeviceManagement=userDeviceManagementRepository
        .findByUserIdAndDeviceUuid(user.getId(), userSignInRequest.getDeviceUuid())
        .orElse(null);
    
    if(userDeviceManagement==null) {
      // create the user device management
      userDeviceManagement=new UserDeviceManagement();
      userDeviceManagement.setUserId(user.getId());
      userDeviceManagement.setDeviceUuid(userSignInRequest.getDeviceUuid());
      userDeviceManagement.setDeviceType(userSignInRequest.getDeviceType());
      userDeviceManagement.setDeviceName(userSignInRequest.getDeviceName());
      userDeviceManagement.setLastLoginTime(LocalDateTime.now());
      userDeviceManagement.setLastLoginIp(userSignInRequest.getLoginIp());
      userDeviceManagement.setLastLoginAddress(userSignInRequest.getLoginAddress());
      
      userDeviceManagement.setTouchIdActive(0);
      userDeviceManagement.setFaceIdActive(0);
      userDeviceManagement.setConfirmedDevice(0);
      userDeviceManagement.setIsDeleted(false);
      
      userDeviceManagementRepository.save(userDeviceManagement);
    } else {
      userDeviceManagement.setLastLoginTime(LocalDateTime.now());
      userDeviceManagement.setLastLoginIp(userSignInRequest.getLoginIp());
      userDeviceManagement.setLastLoginAddress(userSignInRequest.getLoginAddress());
      userDeviceManagementRepository.save(userDeviceManagement);
    }
    
    if((Boolean.TRUE.equals(user.getEnableTwoStepVerification())
        || Integer.valueOf(0).equals(userDeviceManagement.getConfirmedDevice())
        || userDeviceManagement.getConfirmedDevice() ==null) && false) {
      // send two-factor verification code
      String twoFactorAuthToken = generateTwoFactoAuthToken();
      mailService.sendTwoFactorAuthMail(user.getEmail(), user.getFirstName(), twoFactorAuthToken);
      
      UserToken userToken=userTokenService.saveUserToken(
          user, 
          UserTokenType.TWO_FACTOR_AUTHENTICATION, 
          UserTokenReceiverType.EMAIL,
          user.getEmail(),
          twoFactorAuthToken);
      
      // generate the token for 2-factor authentication
      Map<String, Object> claims = new LinkedHashMap<String, Object>();
      claims.put(DEVICE_UUID, userDeviceManagement.getDeviceUuid());

      String token = Jwts.builder()
        .setClaims(claims)
        .setSubject(Long.toString(user.getId()))
        .setIssuedAt(new Date(System.currentTimeMillis()))
        .setExpiration(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
        .signWith(SignatureAlgorithm.HS256, TWO_FACTOR_JWT_TOKEN_SECRET)
        .compact();
      
      UserSignInResponse userSignInResponse=new UserSignInResponse();
      userSignInResponse.setTwoFactorAuthToken(token);
      userSignInResponse.setRequireTwoFactorAuth(true);
      return userSignInResponse;
    }
    
    // delete the old device sessions
    List<UserDeviceSession> userDeviceSessions = userDeviceSessionRepository.findAllByDeviceUuid(userDeviceManagement.getDeviceUuid());
    userDeviceSessionRepository.deleteAll(userDeviceSessions);
    
    // create the new device session
    String token=UUID.randomUUID().toString();
    UserDeviceSession userDeviceSession=UserDeviceSession.builder()
        .key(token)
        .deviceUuid(userDeviceManagement.getDeviceUuid())
        .userId(user.getId())
        .sessionKey(token)
        .createTime(LocalDateTime.now())
        .expirationTime(LocalDateTime.now().plus(USER_SESSION_EXPIRATION_TIME, ChronoUnit.MILLIS))
        .build();
    
    userDeviceSessionRepository.save(userDeviceSession);
    
    UserSignInResponse userSignInResponse=new UserSignInResponse();
    userSignInResponse.setUser(user);
    userSignInResponse.setToken(token);
    userSignInResponse.setRequireTwoFactorAuth(false);
    
    return userSignInResponse;
  }
  
  @Transactional
  public void signOut() {
    // get token from AuthContext
    AuthContext authContext = AuthContext.get();
    String token = authContext.getToken();
    
    // delete session
    userDeviceSessionRepository.deleteById(token);
  }
  
  @Transactional
  public UserSignInResponse twoFactorAuth(TwoFactorAuthRequest twoFactorAuthRequest) {
    // verify the token
    String token = twoFactorAuthRequest.getToken();
    Jws<Claims> jws=Jwts.parser()
        .setSigningKey(TWO_FACTOR_JWT_TOKEN_SECRET)
        .parseClaimsJws(token);
    String userId=jws.getBody().getSubject();
    String deviceUuid=String.valueOf(jws.getBody().get(DEVICE_UUID));
    if(jws.getBody().getExpiration().before(new Date())) {
      // expired
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Token already expired");
    }
    
    // find user
    User user = findUserByUserId(Long.parseLong(userId));
    
    // verify the code
    String verifyCode = twoFactorAuthRequest.getVerifyCode();
    String tokenKey=userId + "-" + UserTokenType.TWO_FACTOR_AUTHENTICATION.getValue();
    UserToken userToken= userTokenService.findByIdOptional(tokenKey)
        .orElseThrow(()->new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid verification code"));
    
    if(!user.getId().equals(userToken.getUserId())) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid verification code");
    }
    
    if(!verifyCode.equals(userToken.getToken())) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Incorrect verification code");
    }

    // create the device session
    String sessionToken=UUID.randomUUID().toString();
    UserDeviceSession userDeviceSession=UserDeviceSession.builder()
        .key(sessionToken)
        .userId(user.getId())
        .deviceUuid(deviceUuid)
        .sessionKey(sessionToken)
        .lastOperationTime(LocalDateTime.now())
        .createTime(LocalDateTime.now())
        .expirationTime(LocalDateTime.now().plus(USER_SESSION_EXPIRATION_TIME, ChronoUnit.MILLIS))
        .build();
    
    userDeviceSessionRepository.save(userDeviceSession);
    
    UserDeviceManagement userDeviceManagement =
        userDeviceManagementRepository.findByUserIdAndDeviceUuid(user.getId(), deviceUuid)
        .orElseThrow(()-> new NotFoundException(BusinessCode.MCU_DEVICE_NOT_FOUND, "Device not found"));
    userDeviceManagement.setConfirmedDevice(1);
    userDeviceManagementRepository.save(userDeviceManagement);
    
    // response
    UserSignInResponse userSignInResponse=new UserSignInResponse();
    userSignInResponse.setUser(user);
    userSignInResponse.setToken(sessionToken);
    userSignInResponse.setRequireTwoFactorAuth(false);
    return userSignInResponse;
  }

  @Transactional
  public User registerUser(RegistrationRequest request) {
    if (userRepository.existsByEmail(request.getEmail())) {
      throw new DuplicateException( BusinessCode.MCU_SAME_EMAIL, "User already exists with this email address");
    }

    User preparedUser =
        User.builder()
            .id(IdGeneratorUtils.getUID())
            .email(request.getEmail())
            .passwordHash(passwordEncoder.encode(request.getPassword()))
            .firstName(request.getFirstName())
            .lastName(request.getLastName())
            .mobilePhone(request.getPhoneNumber())
            .countryName("United States")
            .language("English")
            .currency("USD")
            .interestedCategoryIds("")
            .userTags("buyer")
            .emailSubscription(request.getSignUpForEmails())
            .smsSubscription(request.getSignUpForTextMessage())
            .joinedMichaelsRewards(request.getSignUpForRewards())
            .accountStatus(UserAccountStatus.ACTIVE.ordinal())
            .mobilePhoneVerified(false)
            .confirmedAccount(false)
            .anonymous(false)
            .loginRetryTimes(0)
            .enableTwoStepVerification(false)
            .build();
    
    final User newUser = userRepository.save(preparedUser);
    return newUser;
  }
  
  /**
   * For test only
   */
  @Transactional
  @Deprecated
  public void deleteUserByEmail(final String email) {
    User user = findUserByEmailOptional(email).orElse(null);
    if(user==null) {
      return;
    }
    userRepository.delete(user);
  }

  @Transactional
  public User confirmUser(final String token) {
    // check the token
    final UserToken userToken = userTokenService
            .findByIdOptional(token)
            .orElseThrow(() -> {
                  throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token");
                });
    if (!userToken.getToken().equals(token)) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token");
    }
    if (userToken.hasExpired()) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token");
    }
    if (!(Integer.valueOf(UserTokenType.CONFIRM_EMAIL.getValue()).equals(userToken.getTokenType()))) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token");
    }
    
    // confirm user
    User user = findUserByUserId(userToken.getUserId());
    user.setConfirmedAccount(true);
    
    userRepository.save(user);
    userTokenService.deleteToken(userToken);
    return user;
  }
  
  @Transactional
  public void resetPassword(ResetPasswordRequest request) {
    // check the token
    UserToken userToken=userTokenService.findByIdOptional(request.getToken())
        .orElseThrow(()-> new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token"));
    if(userToken.hasExpired()) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token");
    }
    if(!(Integer.valueOf(UserTokenType.RESET_PASSWORD.getValue()).equals(userToken.getTokenType()))) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token");
    }
    
    // reset password
    User user = findUserByUserId(userToken.getUserId());
    user.setPasswordHash(passwordEncoder.encode(request.getPassword()));
    userRepository.save(user);

    // delete the token
    userTokenService.deleteToken(userToken);
  }

  public void verifyResetPasswordToken(String token) {
    UserToken userToken=userTokenService.findByIdOptional(token)
        .orElseThrow(()-> new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token"));
    
    if(userToken.hasExpired()) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token");
    }
    
    if(!(Integer.valueOf(UserTokenType.RESET_PASSWORD.getValue()).equals(userToken.getTokenType()))) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Invalid token");
    }

    Long userId=userToken.getUserId();
    User user=findUserByUserId(userId);
    if(!Boolean.TRUE.equals(user.getConfirmedAccount())) {
      throw new BadRequestException(BusinessCode.MCU_USR_NOT_CONFIRMED, "User account was not confirmed, please confirm the account first");
    }
  }

  @Transactional
  public void updatePassword(String password, String newPassword) {
    // find the user
    AuthContext authContext = AuthContext.get();
    User user = findUserByUserId(authContext.getUserId());
    
    if (!passwordEncoder.matches(password, user.getPasswordHash())) {
      throw new InvalidCredentialsException(BusinessCode.MCU_EMAIL_PASSWORD_WRONG, "Email or password does not match our records");
    }
    
    user.setPasswordHash(passwordEncoder.encode(newPassword));
    userRepository.save(user);
  }
  
  @Transactional
  public User updateUserProfile(UpdateUserProfileRequest request) {
    AuthContext authContext = AuthContext.get();
    User user = findUserByUserId(authContext.getUserId());

    user.setUserAvatarUrl(request.getUserAvatarUrl());
    user.setFirstName(request.getFirstName());
    user.setLastName(request.getLastName());
    user.setDateOfBirth(LocalDate.parse(request.getDateOfBirth(), DateTimeFormatter.ofPattern("MM/dd/yyyy")));
    user.setMobilePhone(request.getMobilePhone());

    return userRepository.save(user);
  }

  @Transactional
  public User updateUserLocalMichaelsStore(String michaelsStoreId) {
    // Auth context
    AuthContext authContext = AuthContext.get();
    User user = findUserByUserId(authContext.getUserId());

    // Update and save
    user.setLocalMichaelsStoreId(michaelsStoreId);
    userRepository.save(user);

    return user;
  }

  @Transactional
  public void updateDefaultShippingAddress(Long userAddressId) {
    UserAddress userAddress =userAddressRepository
            .findById(userAddressId)
            .orElseThrow(() -> new NotFoundException(BusinessCode.MCU_ADDRESS_NOT_FOUND, "Address is not found"));
    
    AuthContext authContext = AuthContext.get();
    User user = findUserByUserId(authContext.getUserId());

    user.setDefaultShippingAddressId(userAddress.getId());
    userRepository.save(user);
  }
  
  @Transactional
  public void clearDefaultShippingAddress() {
    AuthContext authContext = AuthContext.get();
    User user = findUserByUserId(authContext.getUserId());

    user.setDefaultShippingAddressId(null);
    userRepository.save(user);
  }

  @Transactional
  public void updateDefaultBillingAddress(Long userAddressId) {
    UserAddress userAddress =userAddressRepository
            .findById(userAddressId)
            .orElseThrow(() -> new NotFoundException(BusinessCode.MCU_ADDRESS_NOT_FOUND, "Address is not found"));
    
    AuthContext authContext = AuthContext.get();
    User user = findUserByUserId(authContext.getUserId());

    user.setDefaultBillingAddressId(userAddress.getId());
    userRepository.save(user);
  }

  @Transactional
  public void clearDefaultBillingAddress() {
    AuthContext authContext = AuthContext.get();
    User user = findUserByUserId(authContext.getUserId());

    user.setDefaultBillingAddressId(null);
    userRepository.save(user);
  }

  @Transactional
  public UserEmailSwap saveUserEmailSwapRequest(Long userId, String oldEmail, String newEmail) {
    UserEmailSwap userEmailSwap =
        UserEmailSwap.builder()
            .userId(userId)
            .oldEmail(oldEmail)
            .newEmail(newEmail)
            .status(0) // valid
            .token(UUID.randomUUID().toString())
            .expirationTime(LocalDateTime.now().plus(UserEmailSwap.EXPIRATION, ChronoUnit.MILLIS))
            .build();
    return userEmailSwapRepository.save(userEmailSwap);
  }

  public void swapUserMail(String oldEmail, String newEmail, String token) {
    UserEmailSwap userEmailSwap = userEmailSwapRepository.findByToken(token)
        .orElseThrow(()-> new NotFoundException(BusinessCode.MCU_TOKEN, "Token not found!"));
    
    if(userEmailSwap.hasExpired()) {
      throw new InvalidCredentialsException(BusinessCode.MCU_TOKEN, "Token expired");
    }
    
    if(!userEmailSwap.getOldEmail().equals(oldEmail)) {
      throw new InvalidCredentialsException(BusinessCode.MCU_EMAIL_NOT_MATCH, "Email mismatch!");
    }
    
    User user=findUserByEmail(oldEmail);
    
    user.setEmail(newEmail);
    userRepository.save(user);
    
    // invalid this request
    userEmailSwap.setStatus(1);
    userEmailSwapRepository.save(userEmailSwap);
  }
  
  @Transactional
  public List<UserDeviceManagement> getUserDeviceMangements() {
    AuthContext authContext = AuthContext.get();
    Long userId = authContext.getUserId();
    
    // find the user device by user id
    List<UserDeviceManagement> userDeviceManagements = userDeviceManagementRepository.findAllByUserId(userId);
    return userDeviceManagements;
  }
  
  @Transactional
  public void deleteUserDeviceManagement(String deviceUuid) {
    AuthContext authContext = AuthContext.get();
    Long userId = authContext.getUserId();
    
    // delete the device
    UserDeviceManagement userDeviceManagement = userDeviceManagementRepository
        .findByUserIdAndDeviceUuid(userId, deviceUuid)
        .orElseThrow(()-> new NotFoundException(BusinessCode.MCU_DEVICE_NOT_FOUND, "Device not found"));
    
    userDeviceManagementRepository.delete(userDeviceManagement);
    
    // delete related session
    List<UserDeviceSession> userDeviceSessions = userDeviceSessionRepository.findAllByDeviceUuid(deviceUuid);
    userDeviceSessionRepository.deleteAll(userDeviceSessions);
    
  }
  
  @Transactional
  public UserDeviceManagement updateUserDeviceManagement(String deviceUuid, UpdateUserDeviceRequest request) {

    AuthContext authContext=AuthContext.get();
    Long userId=authContext.getUserId();
    
    UserDeviceManagement userDeviceManagment = userDeviceManagementRepository.findByUserIdAndDeviceUuid(userId, deviceUuid)
      .orElseThrow(()-> new NotFoundException(BusinessCode.MCU_DEVICE_NOT_FOUND, "Device not found"));
    
    if(!userId.equals(userDeviceManagment.getUserId())) {
      throw new BadRequestException(BusinessCode.MCU_DEVICE_NOT_BELONG, "This device does not belong to current user");
    }
    
    userDeviceManagment.setTouchIdActive(request.getTouchIdActive());
    userDeviceManagment.setFaceIdActive(request.getFaceIdActive());
    userDeviceManagementRepository.save(userDeviceManagment);
    
    return userDeviceManagment;
  }



  private String generateTwoFactoAuthToken() {
    String token=String.format("%06d", Math.round(Math.random() * 999999));
    return token;
  }
  
  private String generateUserDeviceSessionKey(Long userId, String deviceUuid) {
    return userId + "-" + deviceUuid;
  }

}
