package com.michaels.mcu.user.service;

import java.util.Optional;

import com.michaels.mcu.shared.common.BusinessCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.michaels.mcu.shared.exception.NotFoundException;
import com.michaels.mcu.user.constants.UserTokenReceiverType;
import com.michaels.mcu.user.constants.UserTokenType;
import com.michaels.mcu.user.entity.User;
import com.michaels.mcu.user.entity.UserToken;
import com.michaels.mcu.user.repository.UserTokenRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserTokenService {

  private final UserTokenRepository userTokenRepository;

  @Autowired
  public UserTokenService(final UserTokenRepository userTokenRepository) {
    this.userTokenRepository = userTokenRepository;
  }
  
  public UserToken findById(String id) {
    UserToken userToken = userTokenRepository.findById(id)
        .orElseThrow(()-> new NotFoundException(BusinessCode.MCU_TOKEN_NOT_FOUND, "User token not found"));
    return userToken;
  }

  public Optional<UserToken> findByIdOptional(String id) {
    Optional<UserToken> optional = userTokenRepository.findById(id);
    return optional;
  }

  public UserToken findByToken(String token) {
    UserToken userToken = userTokenRepository.findByToken(token)
        .orElseThrow(()-> new NotFoundException(BusinessCode.MCU_TOKEN_NOT_FOUND,"User token not found"));
    return userToken;
  }

  public Optional<UserToken> findByTokenOptional(String token) {
    Optional<UserToken> optional = userTokenRepository.findByToken(token);
    return optional;
  }


  @Transactional
  public UserToken saveUserToken(
      final User user, 
      final UserTokenType tokenType, 
      final UserTokenReceiverType userTokenReceiverType,
      final String receiverAddress,
      final String token
      ) {
    // generate the key
    String key;
    switch(tokenType) {
    case CONFIRM_EMAIL:
    case RESET_PASSWORD:
      key=token;
      break;
    case TWO_FACTOR_AUTHENTICATION:
    case LINK_REWARD:
      key=String.format("%d-%s", user.getId(), tokenType.ordinal());
      break;
    default: 
      key=token;
    }
    
    // address
    String tokenReceiverAddress=null;
    switch(userTokenReceiverType) {
    case EMAIL:
      tokenReceiverAddress = user.getEmail();
    case PHONE:
      tokenReceiverAddress = user.getMobilePhone();
    }
    
    // save
    final UserToken userToken =
        UserToken.builder()
          .key(key)
          .userId(user.getId())
          .tokenType(tokenType)
          .token(token)
          .receiverType(userTokenReceiverType)
          .receiverAddress(tokenReceiverAddress)
          .build();
    userTokenRepository.save(userToken);
    
    return userToken;
  }
  
  @Transactional
  public void deleteToken(UserToken userToken) {
    userTokenRepository.delete(userToken);
  }
  
  @Transactional
  public void deleteTokenById(String id) {
    userTokenRepository.deleteById(id);
  }
}
